import pyshark;import requests;import pandas as pd;import io;from datetime import datetime, timedelta;
from netaddr import IPAddress;import pycountry;import os;from datetime import datetime
import pycountry_convert as pc;global devices;import json
NetworkID = "10.0.";continent_map = {"OC":0, "AS": 1, "RU": 2, "AF":3, "EU": 4, "SA": 5, "NA":6}
# Devices -> Key = File Number, Value = Last two bytes of IP address.
def getInternalDevices():
    D={};D[3]=128.152;D[2]=242.29;D[1]=242.46;return D # Dynamic mechinism for devices can be added later
def GetID(dictionary, value):
    for key, val in dictionary.items():
        if str(val) == value:
            return key
    return None
def TimeDifference(Time1, Time2):
    Time1 = datetime.strptime(str(Time1), "%Y-%m-%d %H:%M:%S")
    Time2 = datetime.strptime(str(Time2), "%Y-%m-%d %H:%M:%S")
    T = abs((Time2 - Time1).total_seconds());return T
def DownloadDB(url):
    if os.path.exists("asn-country-ipv4.csv"):
        lastUpdated = open("IP2LocDBDT.bat", "r");downloadDT = lastUpdated.read();lastUpdated.close()
        currentDT = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
        D = TimeDifference(datetime.strptime(downloadDT, "%Y-%m-%d %H:%M:%S"), currentDT)
        print ("This is the difference", D)
        if D>86400:
            print(f"Downloading IP database from {url}")
            response = requests.get(url);response.raise_for_status();savefile = open("asn-country-ipv4.csv", "w")
            savefile.write(response.text);savefile.close()
            downloadDT = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
            lastUpdated = open("IP2LocDBDT.bat", "w");lastUpdated.write(downloadDT);lastUpdated.close()
            return pd.read_csv(io.StringIO(response.text), header=None, names=['start', 'end', 'country_code'])
        else:
            return pd.read_csv("asn-country-ipv4.csv", header=None, names=['start', 'end', 'country_code'])
    else:
        print(f"Downloading IP database from {url}")
        response = requests.get(url);response.raise_for_status();savefile = open("asn-country-ipv4.csv", "w")
        savefile.write(response.text);savefile.close();downloadDT = datetime.strftime(datetime.now(), "%Y-%m-%d %H:%M:%S")
        lastUpdated = open("IP2LocDBDT.bat", "w");lastUpdated.write(downloadDT);lastUpdated.close()
        return pd.read_csv(io.StringIO(response.text), header=None, names=['start', 'end', 'country_code'])
def IP2CC(ip_address, ip_ranges):
    ip = IPAddress(ip_address)
    for entry in ip_ranges:
        if ip >= entry['range'][0] and ip <= entry['range'][1]:
            country_code = entry['country_code']
            country = pycountry.countries.get(alpha_2=country_code)
            if country:
                if country=="RU":
                    continent_code=country
                else:
                    try:
                        continent_code = pc.country_alpha2_to_continent_code(country.alpha_2)
                        return country.name, continent_code
                    except KeyError:
                        return country.name, False
            return 'Unknown Country', False
    return 'Private or Unlisted IP', None
def UpdataDB(csv_url):
    df = DownloadDB(csv_url)
    ip_ranges = [{'range': (IPAddress(row['start']), IPAddress(row['end'])), 'country_code': row['country_code']} for _, row in df.iterrows()]
    return ip_ranges
def start_capture(interface_name, ipv4_csv_url):
    global devices;DevFiles = {};devices=getInternalDevices()
    DefaultLocations = {"0":False, "1":False, "2":False, "3":False, "4":False, "5":False, "6":False}
    for deviceID in devices.keys():
        tmp = open("jsonstore/"+str(deviceID)+".json", "r");DevFiles[deviceID]=json.loads(tmp.read())
        if isinstance(DevFiles[deviceID]["Start"]["DataStorage"]["Location"], int):
            DevFiles[deviceID]["Start"]["DataStorage"]["Location"]=DefaultLocations
        tmp.close()
    devicesIP = [NetworkID+str(x) for x in devices.values()]
    print ("System ready for the following devices:", devices);print ("Monitoring IPs: ", devicesIP)
    ipv4_ranges = UpdataDB(ipv4_csv_url);last_updated = datetime.now()
    cap = pyshark.LiveCapture(interface=interface_name, only_summaries=True, use_json=True)
    print ("One the go now...")
    for packet in cap.sniff_continuously():
        if datetime.now() - last_updated > timedelta(hours=24):
            ipv4_ranges = UpdataDB(ipv4_csv_url);last_updated = datetime.now()
        if hasattr(packet, 'ip'):
            dst_ip_address = packet.ip.dst;src_ip_address = packet.ip.src
            if (src_ip_address in devicesIP or dst_ip_address in devicesIP):
                if (src_ip_address in devicesIP):
                    ip2look = dst_ip_address;Direction="Outgoing"
                    DevID = ".".join(src_ip_address.split(".")[-2:])
                else:
                    ip2look = src_ip_address;Direction="Incoming"
                    DevID = ".".join(dst_ip_address.split(".")[-2:])
                country, continent = IP2CC(ip2look, ipv4_ranges)
                if continent:
                    Loc = continent_map[continent];DevID = GetID(devices, DevID)
                    if (DevFiles[DevID]["Start"]["DataStorage"]["Location"][str(Loc)]==False):
                        if (Direction=="Outgoing"):
                            print(f"Outgoing packet from Device:{DevID} -> External IP:{ip2look} [{country}:{continent}({Loc})]")
                        else:
                            print(f"Incoming packet to   Device:{DevID} <- External IP:{ip2look} [{country}:{continent}({Loc})]")
                        DevFiles[DevID]["Start"]["DataStorage"]["Location"][str(Loc)]=True
                        f = open("jsonstore/"+str(DevID)+".json", "w");f.write(json.dumps(DevFiles[DevID]));f.close()
if (__name__=="__main__"):
    interface_name = 'eth0';print (f"Starting capturing interface {interface_name}, please wait...", )
    ipv4_csv_url = 'https://cdn.jsdelivr.net/npm/@ip-location-db/asn-country/asn-country-ipv4.csv'
    start_capture(interface_name, ipv4_csv_url)