# MQTT Subscriber Code
# To be placed on the Raspberry Pi, to work with the PrC Arduino Shield

import paho.mqtt.client as mqtt
import json


#MQTT Configuration:

print("[PrC Bridge Subscriber] - Starting")

MQTTbroker = "192.168.137.223"
MQTTport = 1883
MQTTusername = "iotlab"
MQTTpassword = "Vancouver3"



def on_connect(client, userdata, flags, rc): #
    print("[PrC Bridge Subscriber Connected] - 'Please ensure the *mqttNetworkBridge_bypass* variable in the 'prc-LAC' code is set to TRUE ")
    client.subscribe("PrC_analysedpackets")

def on_message(client, userdata, msg): #On recieved message over MQTT broker etc.
    
    print("[PrC Bridge Subscriber] - Packet Recieved")
    
    try:
        decodedJSON = str(msg.payload.decode("utf-8","ignore"))
        MSGDictionary = json.loads(decodedJSON)
        print("  ")
        print("Device Name :"+str(MSGDictionary["DN"]))
        print("Device Location: "+str(MSGDictionary["DL"]))
        print("Data Type: "+str(MSGDictionary["DT"]))
        print("Notes: "+str(MSGDictionary["notes"]))
        
    except:
        print(f"Message Recieved [{msg.topic}]:{msg.payload}")

    #LCD_Screen(MSGDictionary["DN"])
    #DataLocation(MSGDictionary["DL"])
    #DataType(MSGDictionary["DT"])
    
    
    
    #return MSGDictionary

def LCD_Screen(deviceName):
    print("LCD Screen: "+deviceName)

def DataLocation(dataLocation):
    print("LCD Screen: "+dataLocation)

def DataType(dataType):
    for typeRendition in dataType:
        print(typeRendition)
        

#Open MQTT Connection (Corresponding to Publisher Code)
     
client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message
client.username_pw_set("iotlab","Vancouver3") #Connection Reset by peer?
#client.connect("test.mosquitto.org",1883)
client.connect(MQTTbroker,1883)
client.loop_forever()

