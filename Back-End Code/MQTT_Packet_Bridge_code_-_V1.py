# pyshark_live-stream

import pyshark
from paho.mqtt import client as mqtt_client
import random
import json
import time

#Capture Settings:
interface="Local Area Connection* 2" #Tells pyShark which interface to sniff packets from (Needs to be the interface that the devices are connected to (ie: the interface relating to the hotspot))

#MQTT Broker Settings
broker = "192.168.137.223" #Set to the IP of the machine hosting the MQTT Broker
port = 1883
client_id = f'python-mqtt-{random.randint(0, 1000)}'
username = 'iotlab' #Related to the Ubuntu Machine in Abacws Lab
password = 'Vancouver3' #^^^
#Static settings: (Do not change, unless matched in 'prc-LAC')
topic = "mqtt_packet_bridge" #Corresponds to the topic used by 'prc-LAC' to view the packets


def connectMQTT(): #Connects to the MQTT Broker, which the 'prc-LAC' code will connect to as well
    global client
    def on_connect(client, userdata, flags, rc):
        if rc == 0: print("[MQTT Packet Bridge Code] - Connected to MQTT Broker")
        else: print("[MQTT Packet Bridge Code] - Failed to connect to MQTT Broker, return code %d\n", rc)
    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password) #Used to access the MQTT Server
    client.on_connect = on_connect
    client.connect(broker, port)
    return client

def MQTTpublish(client, JSONFile, topic): #Used to publish the packets via MQTT, according to the topic
    result = client.publish(topic, JSONFile, 1)
    status = result[0]
    if status == 0: print("[MQTTPublish] - Packet Sent")
    else: print(f"[MQTTPublish] - Failed to send message to topic {topic}")

def startNetworkCapture(): #This is used to sniff network packets off of the specified interface in the top MQTT configuration settings
    while ("a" == "a"):
        for packet in capture.sniff_continuously(packet_count=4):
            try:
                packetInfo = packet.ip._all_fields
                structuredPacket = {}
                demopacket = ""
                timeComponent = packet.sniff_time

                #The code below will put packet information together in a string, to be split in the 'prc-LAC' code:

                demopacket += (str(packetInfo["ip.dst"]) + "|")
                demopacket += (str(packetInfo["ip.src"]) + "|")
                demopacket += (str((str(timeComponent.hour) + ":" + str(timeComponent.minute) + ":" + str(timeComponent.second))) + "|")
                demopacket += (str((packet.ip.proto.showname_value).split(" ")[0]) + "|")
                demopacket += (str(packet.length) + "|")

                print(demopacket)
                client.publish("mqtt_network_bridge",demopacket)
            except:
                print ("[pyShark Live-Stream] Packet Info Missing \n")

mqttconnected = False
while (mqttconnected == False): #This will try to reconnect to the MQTT broker until stopped
    try:
        connectMQTT()
        mqttconnected = True
    except Exception as e:
        print("[MQTT Packet Bridge Code] - "+str(e))

print("[pyShark Live-Stream] SERVICE STARTING")
capture = pyshark.LiveCapture(interface)
print("[pyShark Live-Stream] CAPTURE STARTED")
capture.sniff(timeout=5)
startNetworkCapture()

