#Lead Analysis file for Privacy Cube
#Developed by D. Anley @ Cardiff University

import random
import time
import csv
import requests
import json
import time
from json import loads
from kafka import KafkaConsumer
import pyshark
from paho.mqtt import client as mqtt_client
import pycountry_convert as pycountryConvert


#IP info store:
IP_LocalStore = {}                                              # Used to store IP Address lookups (Saves time on performance and resets after each program launch)
IP_List =[]                                                     # Stores the IP Addresses of devices that are appearing in packet analysis
packetCount = 0                                                 # Used to count how many packets have been filtered through

# Startup Components:

def startNetworkCapture():   #Used to capture packets from a specified interface

    #Config:
    interfaceName = "wlp2s0"    #This interface needs to reflect the hotspot that the devices are connected to (Copy interface from WireShark)

    capture = pyshark.LiveCapture(interfaceName)    
    capture.sniff(timeout=5)
    while ("a" == "a"):
        print(" [startNetworkCapture] - Service Starting")
        for packet in capture.sniff_continuously(packet_count=1):

            #Finding more info from the packets:

            #print(packet)
            print(dir(packet))
            print(str(packet.description))
            #print(packet._packet_string)
            #print(packet.ip._all_fields)

            try:
                packetInfo = packet.ip._all_fields
                structuredPacket = {}

                #Formated Packet Construction:

                timeComponent = packet.sniff_time

                structuredPacket["ip_dst"] = str(packetInfo["ip.dst"])
                structuredPacket["ip_src"] = str(packetInfo["ip.src"])
                structuredPacket["time"] = (str(timeComponent.hour) + ":" + str(timeComponent.minute) + ":" + str(timeComponent.second))
                structuredPacket["protocol"] = str(packet.ip.proto.showname_value).split(" ")[0]
                #Protocol info could be more accurate using: str(packet.ip.proto.showname_value)


                structuredPacket["length"] = str(packet.length)
                structuredPacket["info"] = "NOT YET STORED"

                #print(structuredPacket)

                IPScanDirector(structuredPacket)

            except:
                print ("[pyShark Live-Stream] Packet Info Missing \n")

def connectMQTT():  # [Facilitates connection to MQTT Broker]

    #config:
    broker = "192.168.137.223" #Needs to match the IP of the machine hosting the MQTT broker
    port = 1883
    client_id = f'python-mqtt-{random.randint(0, 1000)}'
    username = 'iotlab' #Matched to Username + Password outlined in Ubuntu Machine MQTT settings (PC central to project)
    password = 'Vancouver3' #^^^

    def on_connect(client, userdata, flags, rc):                    # RC = Return Code
        if rc == 0: print(" [START-UP] - MQTT Connected")
        else: print(" [START-UP] - !! MQTT NOT CONNECTED !! %d\n", rc)
    client = mqtt_client.Client(client_id)
    client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)

    return client

# IP Related code:

def IP_Lookup_service(IPAddress):  # [Uses free website to convert IP Address]
    IPResult = "Not found"                                      #Geolocation default response if IP not found
    IPLookupCount = 0
    while (IPLookupCount <= 1):
        request_url = 'https://geolocation-db.com/jsonp/' + IPAddress
        try:                                                    #Prevents time-out crash
            IPService_response = requests.get(request_url)
            IPResult = IPService_response.content.decode()
            IPResult = IPResult.split("(")[1].strip(")")
            IPResult  = json.loads(IPResult)                    
            IPLookupCount = 1.49                                #Second IP Lookup not required (Use anything above 1 to distinguish)
        except:
            print("[IP_Lookup_Service] - Lookup Error")         #Most Likely a time-out crash
            IPLookupCount += 1                                  #Will run the IP lookup a second time (Or finish if already retried)
            print("[IP_Lookup_Service] - Retrying for '"+IPAddress+"'")
    return(IPResult)

def IP_Lookup(IPAddress):  # [Returns the Region of the IP] 
    global IP_LocalStore                                        
    IPCountryCode = ""                                          #The IP Address to be looked up
    if (IPAddress not in IP_LocalStore):                        #Checks for existing IP Lookup - [PIVOT POINT]
        IPresult = IP_Lookup_service(IPAddress)
        if IPresult != "Not Found":
            IP_LocalStore[IPAddress] = IPresult["country_code"] #Store the IP Address with lookup result to increase performance time
            IPCountryCode = IPresult["country_code"]
            if (IPCountryCode == "RU"): 
                return IPCountryCode    #Returns IP Region as RU ~ Russia
            try:
                return(pycountryConvert.country_alpha2_to_continent_code(IPCountryCode)) #Returns the IP Region - EU for Example
            except:
                pass
                #print("[IP_Lookup - Continent Conversion Error ~ "+IPCountryCode)
        else: 
            IPCountryCode = "N/A"
    else:                                                       #IP Address already found in LocalStore
        IPCountryCode = IP_LocalStore[IPAddress]
        try:
            return(pycountryConvert.country_alpha2_to_continent_code(IPCountryCode))
        except:
            #print("[IP_Lookup - Continent Conversion Error ~ "+IPCountryCode)
            return("N/A")

# MQTT Related code:

def JSONpublish(client, JSONFile, topic): #Publishes MQTT Message to client, using JSON file format
    result = client.publish(topic, str(JSONFile))
    status = result[0]
    if status == 0: print(" MQTT JSON Sent successfully")
    else: print(f"Failed to send message to topic {topic}")

def STANDARDpublish(client, message, device, topic): #Used for a standard MQTT message to be published, in the program for potential future-use
    result = client.publish(topic, message)
    status = result[0]
    if status == 0: print(device+" MQTT Message Sent")
    else: print(f"Failed to send message to topic {topic}")

def prcStructure(deviceName, data_location, dataTypes, dataAccess, dataUsage, notes): # [Creates the PrC JSON File]
    prcDictionary = {"DN":deviceName,"DL":data_location,"DT":dataTypes,"DA":dataAccess,"DU":dataUsage, "notes":notes}
    MQTT_JSON = json.dumps(prcDictionary)
    return MQTT_JSON

# Packet Analysis:

def Amazon_EchoStudio_Analysis(packet):    # [ Packet Analysis for EchoStudio] - Please inspect for more detail on how to build analysis function
    global packetCount
    notes = "N/A" #Used for Human-Interaction, used to specify what device feature is being seen through the packet analysis

    deviceName = "Echo Studio" #The device name that will be flagged up, alongside the analysis info
    print("Echo Studio detected") 
    dataTypes = []  #Stores the datatypes used, which will change depending on which feature is flagged
    
    dataLocation = str(IP_Lookup(packet["ip_dst"])) #Gets the reigon code from the IP_lokup function
    if (dataLocation) == "None":
        dataLocation = str(IP_Lookup(packet["ip_src"])) #Used as a backup method

    if dataLocation != "None": #Provided a location was found from the IP address:
        time = str(packet["time"]) #Get the time info
        if (str(packet["length"]) == "66") and str(packet["protocol"]) == "TCP": #If the packet length is 66 and the protocol is TCP, this conditional statement suggests that the packet was Stay-Alive
            notes = (str(time)+" : [Stay Alive]") #Add the notes so for human-inspection with the time of capture
            dataTypes = ["DT-us"] #The data type, is simply just usage (See README for key)
            print(notes) #Outputs the info to the console
        
        elif str(packet["protocol"]) == "TCP" and (packet["length"] == "1514" or str(packet["length"]) == "184"):
            notes = (time+" : [Alexa Request]")
            print(notes)
            dataTypes = ["DT-us","DT-au"]

        if (notes != "N/A"): #If the packet was tied to a device-feature:
            JSONFile = prcStructure(deviceName, dataLocation, dataTypes, "DA-N/A", "DU-N/A",notes) #This will build the analysed packet structure
            print(str(JSONFile)) #For human-inspection
            JSONpublish(MQTTclient, JSONFile, "PrC_analysedpackets") #MQTT Publish, which can be be viewed by a subscriber (PrC Bridge Subscriber)
        else: #If the packet was NOT tied to a device feature:
            notes = packet["time"]+"GENERAL TRANSMISSION" 
            JSONFile = prcStructure(deviceName, dataLocation, dataTypes, "DA-N/A", "DU-N/A",notes)
            JSONpublish(MQTTclient, JSONFile, "PrC_analysedpackets")#MQTT Publish, to connect to Home Assistant
    else:
        print("IP Lookup Error")


def Google_BlinkMini_Analysis(packet):     # [ Packet Analysis for BlinkMini]
    global packetCount
    #Please note, the anaylsis for the Blinkmini was based on a PCAP file from WireShark
    # This requires the packet info to be derrived from 'packet' in the startNetworkCapture() procedure
    # Old Structure : # [ Connection Type | Address Type | Protocol | Source | Destination | ? | Packet Length ]

    print("BlinkMini Detected")

    notes = "N/A"
    deviceName = "Blink Mini"
    dataTypes = []
    
    dataLocation = str(IP_Lookup(packet["ip_dst"]))
    if (dataLocation) == "None":
        dataLocation = str(IP_Lookup(packet["ip_src"]))

    time= str(packet[1])
    if ((packet["protocol"]) == "TCP" and ("TCP Keep-Alive ACK" in packet["info"])):
        notes = (time+"                 "+deviceName+" : [Stay Alive]")
        dataTypes = ["DT-us"]
    
    elif (packet["protocol"] == "DNS" and "M2-cam-prde.immedia-semi.com" in packet["info"]):
        notes = (time+"                 "+deviceName+" : [Photo Capture Attempt] ") #Further Analysis Required
        dataTypes = ["DT-us","DT-vi"]


    
    if (notes != "N/A"):
        JSONFile = prcStructure(deviceName, dataLocation, dataTypes, "DA-N/A", "DU-N/A",notes)
        print(str(JSONFile))
        JSONpublish(MQTTclient, JSONFile, "PrC_analysedpackets")#MQTT Publish, to connect to Home Assistant
    else:
        notes = packet["time"]+"GENERAL TRANSMISSION"
        JSONFile = prcStructure(deviceName, dataLocation, dataTypes, "DA-N/A", "DU-N/A",notes)
        JSONpublish(MQTTclient, JSONFile, "PrC_analysedpackets")#MQTT Publish, to connect to Home Assistant

def RoboRock_S5_Analysis(packet): # [ WS Packet Analysis for RoboRock S5 ]
    global packetCount 
    detailedTopic="home-assistant"
    notes = "N/A"

    print("RoboRock S5 detected")
    
    deviceName="RoboRockS5"
    dataTypes = []

    #Looking up the IP Location code

    dataLocation = str(IP_Lookup(packet["ip_dst"]))
    if (dataLocation) == "None":
        dataLocation = str(IP_Lookup(packet["ip_src"]))


    #Packet Analysis

    time = str(packet["time"])

    if (str(packet["length"]) == "121" and str(packet["protocol"]) == "UDP"):
        notes = (time+" -                "+deviceName+" : [Stay Alive]") #Given at 5 Second Intervals
        dataTypes = ["DT-sa",str(packetCount)] #STAY ALIVE

    elif (str(packet["protocol"]) == "DNS"):
        notes = (time+" -                "+deviceName+" : [DNS Used to access Xiaomi Website] ")
        dataTypes = ["DT-en",str(packetCount)]
    else: #General Packet Transmission
        dataTypes = ["DT-us",str(packetCount)]
        notes = packet["time"]+" General Transmission"
        
    

    if (notes != "N/A"):
        JSONFile = prcStructure(deviceName, dataLocation, dataTypes, "DA-N/A", "DU-N/A",notes)
        print(str(JSONFile))
        JSONpublish(MQTTclient, JSONFile, "PrC_analysedpackets")#MQTT Publish, to connect to Home Assistant
    else:
        notes = packet["time"]+"GENERAL TRANSMISSION"
        JSONFile = prcStructure(deviceName, dataLocation, dataTypes, "DA-N/A", "DU-N/A",notes)
        JSONpublish(MQTTclient, JSONFile, "PrC_analysedpackets")#MQTT Publish, to connect to Home Assistant

def IPScanDirector(packet): # Can be adapted to register new device on the network

    #print(packet["ip_src"])
    deviceName = ""
    IPLookupStore =[]
    
    Amazon_EchoStudio_IPList = ["192.168.137.106"] #TO BE UPGRADED
    RoboRock_S5_IPList = ["192.168.137.107"] #TO BE UPGRADED



    if (packet["ip_src"] in Amazon_EchoStudio_IPList or packet["ip_dst"] in RoboRock_S5_IPList): 
        print("echospark!")
        Amazon_EchoStudio_Analysis(packet)
    elif (packet["ip_src"] in RoboRock_S5_IPList or packet["ip_dst"] in RoboRock_S5_IPList): RoboRock_S5_Analysis(packet)
    elif (packet["ip_src"] in Amazon_EchoStudio_IPList or packet["ip_dst"] == "192.168.137.111"): Google_BlinkMini_Analysis(packet) #TO BE UPGRADED

# Additional Materials:

MSGcount = 0 #Used to count the number of MQTT packets recieved

def on_message(client, userdata, msg): #On recieved message over MQTT broker etc. #MQTT DEMO
    global MSGcount

    MSGcount += 1 #Used to display how many messages from MQTT broker have been recieved

    if (MSGcount % 20 == 0):
        print("Messages recieved through MQTT: "+str(MSGcount))

    #Packets recieved through MQTT Avenue ARE NOT THE SAME AS PACKETS SNIFFED THROUGH 'startNetworkCapture'
    #The structure is defined on the machine hosting the packet sniffing program
    #Splitting the string between '|' will give the data values as outline below:
    
    sentPacket = msg.payload

    splitPacket = str(sentPacket).split("|")
    structuredPacket = {}

    structuredPacket["ip_dst"] = splitPacket[0]
    structuredPacket["ip_src"] = splitPacket[1]
    structuredPacket["time"] = splitPacket[2]
    structuredPacket["protocol"] = splitPacket[3]
    structuredPacket["length"] = splitPacket[4]
    structuredPacket["info"] = "NOT YET STORED"

    IPScanDirector(structuredPacket)

#Starup Sequence:

# - Ensure the MQTT broker is running (Mosquitto Based for intial project)
# - Ensure the correct network interface has been input, for the 'startNetworkCapture' procedure (The hotspot the devices are connected to)

mqttNetworkBridge_bypass = False #CHANGE TO FALSE IF STREAMING FROM THIS MACHINE
MQTTConnectionEstablished = True # !!!!!!!!!!!!!!1

while MQTTConnectionEstablished == False: #Will try and connect to broker until the program is stopped
    try:
        MQTTclient = connectMQTT() #Connects to the MQTT broker, which supports communication to the privacy cube     
        MQTTConnectionEstablished = True
    except Exception as e:
        print(" [MQTT Connection] - "+str(e))
        time.sleep(1)

if (mqttNetworkBridge_bypass == True):
    print("Receiving packets through MQTT Connection.")
    MQTTclient.on_message = on_message
    MQTTclient.subscribe("mqtt_network_bridge")

elif(mqttNetworkBridge_bypass == False):
    print("Using 'in-program' packet capture.")
    startNetworkCapture()

MQTTclient.loop_forever()  #Used to keep the MQTT Client running

    


startNetworkCapture()






