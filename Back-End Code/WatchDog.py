import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler
import socket, json
ARDUINO_IP = '10.0.241.247'
ARDUINO_PORT = 8888
global DevFiles
DevFiles = {}
for x in range(0,32):
    DevFiles["jsonstore/"+str(x)+".json"]={}
def convert_value(value):
    if isinstance(value, bool):
        return '1' if value else '0'
    elif isinstance(value, int):
        return str(value).zfill(2)
    elif isinstance(value, str):
        return value
    else:
        return None
def convert_to_string(data):
    result = ''
    for key, value in data.items():
        if isinstance(value, dict):
            result += convert_to_string(value)
        elif isinstance(value, list):
            result += ''.join([convert_value(item) for item in value])
        else:
            result += convert_value(value)
    return result
def PrCDeviceUpdate(JSON_FILE_PATH, host, port):
    global DevFiles
    with open(JSON_FILE_PATH, 'r') as file:
        data = convert_to_string(json.load(file))
        print (data)
        if (data != DevFiles[JSON_FILE_PATH]):
            DevFiles[JSON_FILE_PATH]=data
            print("Device", JSON_FILE_PATH, "file is changed, updating PrivacyCube.")
            with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
                try:
                    s.settimeout(10);s.connect((host, port));s.sendall((data + '\n').encode('utf-8'));
                except socket.error:
                    print ("Error: No response from PrivacyCube")
                s.close()
                # response = b""
                # while True:
                #     d = s.recv(4)
                #     if not d:
                #         break
                #     response += d
                # RET=response.decode().replace('\r\n', '')
                # s.close()
                # print (RET)
                # if (RET=="OK"):
                #     print ("Success: Status Updated to PrivacyCube")
                #     return True
                # elif (RET=="NA"):
                #     return False
        else:
            print("Device", JSON_FILE_PATH, "file is changed, BUT NOT CONTENT.")
            return True
def on_modified(event):
    if not event.is_directory:
        file_path = event.src_path
        PrCDeviceUpdate(file_path, ARDUINO_IP, ARDUINO_PORT)
        time.sleep(5)
def watch_directory(path):
    event_handler = FileSystemEventHandler()
    event_handler.on_modified = on_modified
    observer = Observer()
    observer.schedule(event_handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
if __name__ == "__main__":
    # On first run, update all devices on Privacy Cube with a delay of 5 seconds
    for x in range(0,31):
        Result = PrCDeviceUpdate("jsonstore/"+str(x)+".json", ARDUINO_IP, ARDUINO_PORT)
        time.sleep(5)
        # print (x, Result)
        # while (Result == False):
        #     Result = PrCDeviceUpdate("jsonstore/"+str(x)+".json", ARDUINO_IP, ARDUINO_PORT)
        #     print (x, Result)
            # time.sleep(5)
        # time.sleep(5)
    path = 'jsonstore'
    watch_directory(path)