<div align="center">
  <a href="https://www.youtube.com/watch?v=1bmnGjqConU" title="YouTube video player">
    <img src="https://img.youtube.com/vi/1bmnGjqConU/0.jpg" alt="YouTube video player">
  </a>
</div>

#Please refer to READMEv2.md for latest documentation.

# PrivacyCube: Data Physicalization for Enhancing Privacy Awareness in IoT
PrivacyCube is an innovative data physicalization tool designed to enhance privacy awareness in smart home settings. It visualizes the consumption of IoT data by presenting privacy-related notifications. The goal of PrivacyCube is to empower smart home residents by enabling them to better understand their data privacy and to facilitate discussions about the data management practices of IoT devices within their homes. By utilizing PrivacyCube, households are equipped to collectively learn about and make informed decisions regarding their privacy.

## [Privacy-Cube - Packet Analysis Code]

The following Python Programs are designed to work together in 'sniffing packets' from smart-devices, analysing them
and pinning them to a 'device feature'. For example, when an Amazon Echo is listening to a response, the relevant
packets are captured and then directed to the analysis function, for the specific device. Through WireShark tests, 
the packet info can built into the analysis function, for the user to know that the device was listening for a response.

## [Development Notes: ]
As of V1, the programs should work as expected, provided the MQTT Broker is working. In building a solution that can be
deployed more easily, PyShark was implemented in place of WireShark + PCAP files, as this facilitates streaming opposed
to slower entire file-reads which would be much further than real-time; an essential consideration for the project.

With the use of PyShark, the program works much better, but some of the analysis (esspecially in future), relies on the
'info' tab provided in WireShark.

Moroever, the analysis completed at the start of this project development has already been made partially redudant, most
notably the Amazon_EchoStudio Stay-Alive analysis, whereby the packets have decreased in length. To ensure this is up-to-date,
testing will need to occur periodically to keep the program working.


## |||[Our Machines]

These programs were designed to work with the Cardiff University, Privacy-Cube project, which aims to better communicate
how data is being used by smart-devices for consumers' privacy. Our current setup involves the following:

### [Ubuntu Machine]:
	Running: 'prc-LAC'

### [Arduino Leonardo with Raspberry Pi expansion]: *Used for the tangible Privacy-Cube device*
	Supported by: 'PrC-Bridge Subscriber' code
	*This code implemented into Arduino code, running directly from the Arduino*

### [Ubuntu Machine]:
	Running:  MQTT Packet Bridge Code
	We are running this program from the Windows machine at current, because the Ubuntu machine does not yet facilitate
	a hotspot for the smart-devices to connect as well as a wireless connection to the internet.

	As seen in the description, this is a work-around method (CHOICE 2), allowing the Windows Machine to run the hotspot
	for the devices and facilitate their connection to the internet, whilst sending the captured packets across to the Ubuntu
	machine via the MQTT Broker.

### [Smart Devices ~ Connected to the Ubuntu Machine hotspot]:
	- Alexa Echo Studio
	- BlinkMini
	- RoboRock S5

## |||[Programs:]

### [PrC.ino]
Arduino code running turning the PrivacyCube lights.


(X) - [prc-LAC] prcCentral Lead Analysis Code:

	This is the main program for capturing 'Smart-Device' network traffic and can run with little set-up. The program
	does the following:

	1) Captures Packets on a specified network using PyShark /OR/ Receive packets communicated over MQTT
	2) Detects if the 'IP Source' or 'IP Destination' relates to any device IPs
	3) Analyses the packets, using a dedicated analysis function for the specific device
	4) Detects a device feature, by comparing against packet information for the feature
	5) Fills the prcStructure, for the Privacy-Cube, with device feature information according to the analysis.
	6) Sends the JSONFile to a MQTT broker (from filled prcStructure), for the Privacy-Cube to view them


	[ 1 - Capturing packets or receiving them from MQTT ]:

	-
	CHOICE 1: 'Using the in-program capture' ~ Preferred
	[ Functions Used: startNetworkCapture() ]

	If the smart-devices are connected to the computer running this program, the network traffic can be viewed
	using PyShark within the program and will make for better performance, as the packets are captured and
	analysed in the same place.

		Under the 'Startup Sequence' comments at the bottom of the code, ensure the following:
		
			mqttNetworkBridge_bypass = FALSE
		
	-
	CHOICE 2: 'Using MQTT to recieve packets' ~ Backup Method, to be used if hotspot isn't an option
	[ Functions Used: on_message() ]

	If you are unable to connect the devices to a hotspot that is hosted on the machine running
	[prc-LAC], you can use a smaller program to capture the packets from another machine and communicate 
	them back to the [prc-LAC] hosting machine.
	
		Under the 'Start-up Sequence' comments at the bottom of the code, ensure the following:
		
			mqttNetworkBridge_bypass = TRUE

	[ 2 - IP Address Routing ]:
	[ Functions used: IPScanDirector() ]
	In the (IPScanDirector(packet)) Function, there is an array that relates to each device that has an analysis
	function.

		__
		'packet IP Addresses' -> IPScanDirector

		Is one of 'IP Source / IP Destination' in (Amazon_EchoStudio_IPArray)?
			| ~Yes~
			V
		__	Start (Amazon_EchoStudio_Analysis) with packet

	IPARRAY:
		*manufacturer_name*_IPArray ~ Array

	Analysis:
		*manufacturer_name*_Analysis() ~ Function
	

	[ 3 - Packet Analysis ]:
	[ Functions used: *manufacturer_name*_Analysis() ]

	With the packet routed to an Analysis Function, it will compare the packet info against requirements to fulfil a
	device feature.

	[ 4 - Detect Device Feature ]:
	[ Functions used: *manufacturer_name*_Analysis() ]

	For example, a stay-alive packet may use the 'UDP Protocol' and have a packet length of '66' at the very least.
	If the packet fulfils those requirements, we can consider it a stay-alive packet.

	[5 - Build prcStructure ]:
	[ Functions used: prcStructure() ]

	We can now build use the prcStructure (a dictionary), to store data about this device feature:

-------[prcStructure]--

		[deviceName] - Whatever the device is referred to as (eg: EchoStudio)
	
		[NOTES] - Used for Human-Inspection and provides the time the packet was captured (NOT ANALYSED)

		[dataTypes (DT-**)] - Used in detailing what type of data is being used, by this feature:
		'lo - Location' , 'pr - Presence' , 'vi - Visual' , 'au - Audio' , 'bi - biometrics'
		'he' - Health' , 'us - Usage (General)' , 'en - Environment'

		[dataLocation (DL-**)] - Shows where the other IP Address is located (USING IP_Lookup function):
		'NA - North America' , 'SA - South America' , 'AF - Africa' , 'EU - Europe'
		'RU - Russia (Dedicated Region for project)' , 'AS - Asia' , 'AU - Australia'

		[dataAccess (DA-**)] - Shows who will have access to this data:
		'RO - Resource Owner' , 'TP - Trusted Party' , 'SP - Service Provider'
		'DM - Device Manufacturer' , 'LE - Law Enforcement' , 'PU - Public'
		'3P - Third Party', 'MO - Marketing Organisation'
		

		[dataUsage (DU-**)] - Shows what the data will be used for:
		'RE - Revenue' , 'AN - Analytics' , 'RE - Research' , 'SU - Surveillance'
		'SE - Security', 'TA - Targeted ADs' , 'LI - Lifestlye' , 'PR - Productivity'


	----


	[6 - MQTT Publish]
	[ Functions used: JSONpublish ]

	With the prcStructure built, the (JSONpublish) function will send the analysis information to the MQTT Broker under
	the 'PrC_analysedpackets' topic.
~~~~~~[ To add a device to the program: ]
	
	In the IPScanDirector, create an array for the devices' IP Address, with an array being used so that multiple devices
	can be detected, provided their IP addresses are present.

		Recommended Structure : *manufacturer*_*device name*_IPArray

	Copy one of the pre-existing functions, such as Amazon_EchoStudio_Analysis:
		- Rename the function
		- Copy an if statement and populate with the relevant function and array, in the IPScanDirector
		- Change the conditional statements with the relevant packet anaylsis


(x) - [MQTT Packet Bridge Code]:

	This program is to be used on a different machine, to the one running the analysis code, to receive packets and send
	them to the MQTT broker. This code was developed as an alternative method to capture packets, whereby the main machine
	cannot hotspot and connect to the internet simultaneously.

	Start-up & Configuration:

	- Match the MQTT Broker settings at the top of the program
		- The machine needs to be on the same network as the MQTT broker
	- Ensure the StartCapture interface matches the interface used for the hotspot

	[ 1 - Connect to MQTT  Broker]
	[Functions used: connectMQTT() ]

	- Connects to the MQTT broker and will return a corresponding message

	[ 2 - Start network capture ]
	[Functions used: startNetworkCapture(), MQTTPublish() ]

	- Starts the network capture, on the specified interface
	- Will send the packets sniffed, to the MQTT broker using the MQTTPublish function, which will be received from the MQTT
		topic of "mqtt_packet_bridge"

(x) - [prC Bridge Subscriber]: *Used to verify MQTT packet transmission*

	This program is used to receive the MQTT packets and print the relevant information. This program was purely written to receive
	the analysed packets through an MQTT subscriber. 

	Start-up & Configuration:

	- Match the MQTT Broker settings at the top of the program
		- The machine needs to be on the same network as the MQTT broker
	
	[ 1 - Connect to MQTT Broker]
	[Functions used: on_connect()]

	- Connects to the MQTT broker and will return a corresponding message.

	[ 2 - Output the received messages]
	[Functions used: on_message()]

	When a packet is sent to the 'PrC_analysedpackets' topic on the MQTT broker, this subscriber will print
	out the analysed packets, from the JSONFile.

## |||[Setting up the programs to run]:

(x) - Analysis Machine: (prc-LAC)
	
	This will analyse the communication of the Smart-Devices using the 'prc-LAC' program. As discussed under the program description,
	there are two methods that the program can use to get the Smart-Device network traffic. Decide which method you are using, either
	'Choice 1' or 'Choice 2'.

	//SEE 'PRC-LAC' for choice setup//

	The Analysis Machine should also host the MQTT broker. For our development we used a mosquito broker, but any broker that works
	across a local network should work with the program. ENSURE THE BROKER SETTINGS MATCH IN EACH OF THE PROGRAMS.

	The program will notify you on the connection status to the MQTT broker, if it doesn't work ensure the following:
		- The machines using any of these programs, are connected to the same network as the machine hosting the MQTT Broker
		- Ensure the IP address in the MQTT configuration settings, match that of the machine running the broker.
		- Ensure the broker allows for connection over the network, not just on the device itself (also, check firewall settings).


(x) - //FOR CHOICE 2 ONLY//: 'Hotspot' Machine (MQTT Packet Bridge Code):
	//
	//When the program is running from the machine, running the hotspot with devices connected ensure the following:
	//- This machine is connected to the same network as the Analysis Machine.
	//- The smart-devices are connected to this machine's hostpot
	//- In the 'MQTT Packet Bridge Code', the interface needs to match the Network Interface working for the hotspot.
	//- Run the program and ensure that the program is connecting to the MQTT Hotspot (The same advice, as for the Analysis Machine)
	/////

	
	(Continuing on the Analysis Machine):

	Provided that one of the choices have been followed, as stated in 'prc-LAC', the setup should work.

(x) - MQTT Receival Machine (PrC Bridge Subscriber):
	
	This program can be run on any machine with Python, which can pick-up the analysed packets, provided the required libraries are installed
	and it is connected to the network running the MQTT Broker.