<div align="center">
  <a href="https://www.youtube.com/watch?v=1bmnGjqConU" title="YouTube video player">
    <img src="https://img.youtube.com/vi/1bmnGjqConU/0.jpg" alt="YouTube video player">
  </a>
</div>

# PrivacyCube: Data Physicalization for Enhancing Privacy Awareness in IoT
PrivacyCube is an innovative data physicalization tool designed to enhance privacy awareness in smart home settings. It visualizes the consumption of IoT data by presenting privacy-related notifications. The goal of PrivacyCube is to empower smart home residents by enabling them to better understand their data privacy and to facilitate discussions about the data management practices of IoT devices within their homes. By utilizing PrivacyCube, households are equipped to collectively learn about and make informed decisions regarding their privacy.

## [PrivacyCube Local Network Monitoring Service (PCLNM)]
This program requires information of the Network ID prefix e.g. "10.0." as well as Device and Host IDs of the devices to be monitored by PrivacyCube e.g. D[1]=128.52 to scan only limited devices for optimised execution, the service also need the internal interface on which the devices are connected e.g. eth0.

### Downloading Latest IP2Location database
The service will download latest ip2location database from 'https://cdn.jsdelivr.net/npm/@ip-location-db/asn-country/asn-country-ipv4.csv' before execution and will try to update every 24 hours.

### [Device Properties (JSON)]
The service will load properties of all devices from .json files in 'jsonstore' directory, each file's name is based on Device ID i.e. Device ID: 1's file will be 1.json. Once there is an update on a device, the json file will be updated by the service.

### Features

- Capture and analyze network packets in real-time
- Identify and log packets based on device IPs
- Map IP addresses to countries and continents
- Update IP-to-country database periodically
- Update JSON file according to the device's connection

## [PrivacyCube's WatchDog]
The WatchDog will load json files for devices and convert the data into string and compress it. For the first time of loading the watchdog, it will connect to PrivacyCube's TCP Server on port 8888 to update all devices one by one and wait for any change in the json files from the intial loading time. If there is any change in the json file, the new compressed string will be updated on PrivacyCube.

### Features

- Monitor JSON files for changes
- Convert JSON data to a specific string format
- Send updates to a remote server (PrivacyCube)
- Handle network communication and errors

## [Arduino Process]
The Arduino code privacycube/Arduino Code/PrC_v5.ino will map all the LED with Device object "devices", upon receiving the compressed string from PCLNM, it will convert it and update the Device object using ID of the devices.

### Features

- Control devices via Nextion touchscreen
- Visual feedback using Grove LED bars and NeoPixel LEDs
- WiFi connectivity for remote control
- JSON-based communication for device status updates

## [Our Machines]

These programs were designed to work with the Cardiff University, Privacy-Cube project, which aims to better communicate
how data is being used by smart-devices for consumers' privacy. Our current setup involves the following:

### [Ubuntu Machine]:
	Operating System: Ubuntu 20.04
    Running Services: DHCP, IPTables (NAT), DNS and PCLNM
    Running Code: WatchDog.py
    *A raspberry pi based on raspbian os can also be used for this purpose*

### [Arduino Uno WiFi]: *Used for the tangible Privacy-Cube device*
	Supported by: 'PrC_v5.ino' code
	*This code implemented into Arduino code, running directly from the Arduino*

### [Smart Devices ~ Connected to the Ubuntu Machine WAP]:

	- Alexa Echo Studio
	- BlinkMini
	- RoboRock S5