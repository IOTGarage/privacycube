#include "Nextion.h"
#include <ChainableLED.h>
#include <Grove_LED_Bar.h>
#include "Adafruit_NeoPixel.h"
#include <SPI.h>
// #include <EEPROM.h>
// #define EEPROM_SIZE 1024
// #define MAX_VALUE_SIZE 2
#define START_OF_MESSAGE "##"
#define END_OF_MESSAGE "!!"
// numpofLEDpixels, Pin, NEO_GRB + NEO_KHZ800 ( sets the bits stream to RBG at 800khz)
Adafruit_NeoPixel pixels(15, 3, NEO_GRB + NEO_KHZ800);
#define NUM_LEDS 23
 //clock pin, data pin, 23 LEDs, port D2
ChainableLED leds(2, 3, NUM_LEDS);
//LED bars D4,D5,D6,D7,D8,A0,A1,A2,A3
 // Grove_LED_Bar: clock pin, data pin, orientation
Grove_LED_Bar LocationBar(5, 4, 0); Grove_LED_Bar PressureBar(6, 5, 0); Grove_LED_Bar VisualBar(7, 6, 0);
Grove_LED_Bar AudioBar(8, 7, 0); Grove_LED_Bar BiometricBar(9, 8, 0); Grove_LED_Bar HealthlBar(A1, A0, 0);
Grove_LED_Bar UsageBar(A2, A1, 0); Grove_LED_Bar EnviromentBar(A3, A2, 0);
// LED Colors
int R=100;int G=0;int B=0; int CurrentDevice=0;
int CurrentPage = 0; uint32_t numbers[32]; uint32_t ButtonPress;
//Living Room Devices
// 0: Camera            4: Light
// 1: Lock              5: Thermostat
// 2: TV                6: Air Purifier
// 3: Voice Assistant   7: Vacuum
NexDSButton L1 = NexDSButton(1, 8, "bt6");    NexDSButton L5 = NexDSButton(1, 6, "bt4");
NexDSButton L2 = NexDSButton(1, 3, "bt0");    NexDSButton L6 = NexDSButton(1, 7, "bt5");
NexDSButton L3 = NexDSButton(1, 9, "bt7");    NexDSButton L7 = NexDSButton(1, 10, "bt2");
NexDSButton L4 = NexDSButton(1, 5, "bt3");    NexDSButton L8 = NexDSButton(1, 4, "bt1");
//Callback functions for LivingRoom Devices
void L1CB(void *ptr){int ID=1;L1.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void L2CB(void *ptr){int ID=2;L2.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void L3CB(void *ptr){int ID=3;L3.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void L4CB(void *ptr){int ID=4;L4.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void L5CB(void *ptr){int ID=5;L5.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void L6CB(void *ptr){int ID=6;L6.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void L7CB(void *ptr){int ID=7;L7.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void L8CB(void *ptr){int ID=8;L8.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
//Bedroom Devices
// 0: Camera            4: Light
// 1: Sleep Monitor     5: Thermostat
// 2: TV                6: Air Purifier
// 3: Voice Assistant   7: Vacuum
NexDSButton BD1 = NexDSButton(2, 9, "bt6");  NexDSButton BD5 = NexDSButton(2, 7, "bt4");
NexDSButton BD2 = NexDSButton(2, 6, "bt0");  NexDSButton BD6 = NexDSButton(2, 8, "bt5");
NexDSButton BD3 = NexDSButton(2, 10, "bt7"); NexDSButton BD7 = NexDSButton(2, 4, "bt2");
NexDSButton BD4 = NexDSButton(2, 5, "bt3");  NexDSButton BD8 = NexDSButton(2, 3, "bt1");
//Callback functions for Bedroom Devices
void BD1CB(void *ptr){int ID=9;BD1.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void BD2CB(void *ptr){int ID=10;BD2.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void BD3CB(void *ptr){int ID=11;BD3.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void BD4CB(void *ptr){int ID=12;BD4.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void BD5CB(void *ptr){int ID=13;BD5.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void BD6CB(void *ptr){int ID=14;BD6.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void BD7CB(void *ptr){int ID=15;BD7.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void BD8CB(void *ptr){int ID=16;BD8.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
//Bathroom Devices
// 0: Mirror           4: Light
// 1: Soap Dispenser   5: Thermostat
// 2: Toothbrush       6: Extractor Fan
// 3: Bath             7: Faucet
NexDSButton T1 = NexDSButton(3, 3, "bt0");  NexDSButton T5 = NexDSButton(3, 9, "bt4");
NexDSButton T2 = NexDSButton(3, 7, "bt1");  NexDSButton T6 = NexDSButton(3, 10, "bt5");
NexDSButton T3 = NexDSButton(3, 8, "bt7");  NexDSButton T7 = NexDSButton(3, 5, "bt2");
NexDSButton T4 = NexDSButton(3, 4, "bt3");  NexDSButton T8 = NexDSButton(3, 6, "bt6");
void T1CB(void *ptr){int ID=17;T1.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void T2CB(void *ptr){int ID=18;T2.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void T3CB(void *ptr){int ID=19;T3.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void T4CB(void *ptr){int ID=20;T4.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void T5CB(void *ptr){int ID=21;T5.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void T6CB(void *ptr){int ID=22;T6.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void T7CB(void *ptr){int ID=23;T7.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void T8CB(void *ptr){int ID=24;T8.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
//Kitchen Devices
// 0: Fridge            4: Light
// 1: Coffee Machine    5: Thermostat
// 2: TV                6: Extractor Fan
// 3: Voice Assistant   7: Faucet
NexDSButton K1 = NexDSButton(4, 3, "bt1");  NexDSButton K5 = NexDSButton(4, 6, "bt4");
NexDSButton K2 = NexDSButton(4, 8, "bt0");  NexDSButton K6 = NexDSButton(4, 7, "bt5");
NexDSButton K3 = NexDSButton(4, 9, "bt7");  NexDSButton K7 = NexDSButton(4, 4, "bt2");
NexDSButton K4 = NexDSButton(4, 5, "bt3");  NexDSButton K8 = NexDSButton(4, 2, "bt6");
void K1CB(void *ptr){int ID=25;K1.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void K2CB(void *ptr){int ID=26;K2.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void K3CB(void *ptr){int ID=27;K3.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void K4CB(void *ptr){int ID=28;K4.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void K5CB(void *ptr){int ID=29;K5.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void K6CB(void *ptr){int ID=30;K6.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void K7CB(void *ptr){int ID=31;K7.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
void K8CB(void *ptr){int ID=32;K8.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID);}else{off();}}
NexPage page0 = NexPage(0, 0, "page0");  // Home Screen
NexPage page1 = NexPage(1, 0, "page1");  // Livingroom
NexPage page2 = NexPage(2, 0, "page2");  // Bedroom
NexPage page3 = NexPage(3, 0, "page3");  // Bathroom
NexPage page4 = NexPage(4, 0, "page4");  // Kitchen
void page0PushCallback(void *ptr){CurrentPage = 0;Serial.print("Page: ");Serial.println("Home");}
void page1PushCallback(void *ptr){CurrentPage = 1;Serial.print("Page: ");Serial.println("Living Room");}
void page2PushCallback(void *ptr){CurrentPage = 2;Serial.print("Page: ");Serial.println("Bedroom");}
void page3PushCallback(void *ptr){CurrentPage = 3;Serial.print("Page: ");Serial.println("Bathroom");}
void page4PushCallback(void *ptr){CurrentPage = 4;Serial.print("Page: ");Serial.println("Kitchen");}
NexTouch *nex_listen_list[] = {
  &L1,  &L2,  &L3,  &L4,  &L4,  &L6,  &L7,  &L8, &BD1,  &BD2,  &BD3,  &BD4,  &BD5,  &BD6,  &BD7,  &BD8,
  &T1,  &T2,  &T3,  &T4,  &T5,  &T6,  &T7,  &T8, &K1,  &K2,  &K3,  &K4,  &K5,  &K6,  &K7,  &K8,
  &page0,  &page1,  &page2,  &page3,  &page4, NULL };
struct Device {
  int deviceID;  String deviceType;  boolean location[7];  int dataRetentionTime;  bool locationEnabled;  bool presenceEnabled;  bool visualEnabled;
  bool audioEnabled;  bool biometricsEnabled;  bool healthEnabled;  bool usageEnabled;  bool environmentEnabled;  bool resourceOwnerAccess;
  bool trustedPartyAccess;  bool serviceProviderAccess;  bool deviceManufacturerAccess;  bool lawEnforcementAccess;  bool publicAccess;
  bool thirdPartyAccess;  bool marketingOrganisationsAccess;  bool revenueUsage;  bool analyticsUsage;  bool researchUsage;  bool surveillanceUsage;
  bool securityUsage;  bool targetedAdsUsage;  bool lifeStyleUsage;  bool productivityUsage;
};
Device devices[32];

void SetupLEDs(int DSBtn){
  Serial.print("Device: "); Serial.println(DSBtn);
  // Data Location and Retention
  if (CurrentDevice==DSBtn){
    for (int i=0; i<=6; i++){if (devices[DSBtn].location[i]){leds.setColorRGB(i, R, G, B);}}
  }
  else{
    off();
    for (int i=0; i<=6; i++){if (devices[DSBtn].location[i]){leds.setColorRGB(i, R, G, B);}}
    for(int i=0; i<devices[DSBtn].dataRetentionTime; i++) {pixels.setPixelColor(i, pixels.Color(R, G, B));pixels.show();}
    // Data Type
    if (devices[DSBtn].locationEnabled){LocationBar.setBits(0x6);}
    if (devices[DSBtn].presenceEnabled){PressureBar.setBits(0x6);}
    if (devices[DSBtn].visualEnabled){VisualBar.setBits(0x6);}
    if (devices[DSBtn].audioEnabled){AudioBar.setBits(0x6);}
    if (devices[DSBtn].biometricsEnabled){BiometricBar.setBits(0x6);}
    if (devices[DSBtn].healthEnabled){HealthlBar.setBits(0x6);}
    if (devices[DSBtn].usageEnabled){UsageBar.setBits(0x6);}
    if (devices[DSBtn].environmentEnabled){EnviromentBar.setBits(0x6);}
    //Data Usage
    if (devices[DSBtn].surveillanceUsage){leds.setColorRGB(7, R, G, B);}
    if (devices[DSBtn].researchUsage){leds.setColorRGB(8, R, G, B);}
    if (devices[DSBtn].analyticsUsage){leds.setColorRGB(9, R, G, B);}
    if (devices[DSBtn].revenueUsage){leds.setColorRGB(10, R, G, B);}
    if (devices[DSBtn].productivityUsage){leds.setColorRGB(11, R, G, B);}
    if (devices[DSBtn].lifeStyleUsage){leds.setColorRGB(12, R, G, B);}
    if (devices[DSBtn].targetedAdsUsage){leds.setColorRGB(13, R, G, B);}
    if (devices[DSBtn].securityUsage){leds.setColorRGB(14, R, G, B);}
    // Data Access
    if (devices[DSBtn].deviceManufacturerAccess){leds.setColorRGB(15, R, G, B);}
    if (devices[DSBtn].serviceProviderAccess){leds.setColorRGB(16, R, G, B);}
    if (devices[DSBtn].thirdPartyAccess){leds.setColorRGB(17, R, G, B);}
    if (devices[DSBtn].resourceOwnerAccess){leds.setColorRGB(18, R, G, B);}
    if (devices[DSBtn].marketingOrganisationsAccess){leds.setColorRGB(19, R, G, B);}
    if (devices[DSBtn].trustedPartyAccess){leds.setColorRGB(20, R, G, B);}
    if (devices[DSBtn].publicAccess){leds.setColorRGB(21, R, G, B);}
    if (devices[DSBtn].lawEnforcementAccess){leds.setColorRGB(22, R, G, B);}
  }
}
void off(){
  LocationBar.setBits(0x0);  PressureBar.setBits(0x0);  VisualBar.setBits(0x0);  AudioBar.setBits(0x0);
  HealthlBar.setBits(0x0);  UsageBar.setBits(0x0);  EnviromentBar.setBits(0x0);  BiometricBar.setBits(0x0);
  for (int i=0; i<=22; i++){ leds.setColorRGB(i, 0, 0, 0);}
  for(int i=0; i<15; i++) { pixels.setPixelColor(i, pixels.Color(0, 0, 0));  pixels.show(); }
}
void startUpTest(){
  LocationBar.setBits(0x1);  delay(500);  LocationBar.setBits(0x0);
  PressureBar.setBits(0x1);  delay(500);  PressureBar.setBits(0x0); 
  VisualBar.setBits(0x1);    delay(500);  VisualBar.setBits(0x0); 
  AudioBar.setBits(0x1);     delay(500);  AudioBar.setBits(0x0);
  BiometricBar.setBits(0x1); delay(500);  BiometricBar.setBits(0x0);
  HealthlBar.setBits(0x1);   delay(500);  HealthlBar.setBits(0x0);
  UsageBar.setBits(0x1);     delay(500);  UsageBar.setBits(0x0);
  EnviromentBar.setBits(0x1);delay(500);  EnviromentBar.setBits(0x0);
  for (int i=0; i<=6; i++){leds.setColorRGB(i, R, G, B);    delay(1000);    leds.setColorRGB(i, 0, 0, 0);}
  for (int i=7; i<=22; i++){leds.setColorRGB(i, R, G, B); delay(500); leds.setColorRGB(i, 0, 0, 0);}
  for(int i=0; i<15; i++) {pixels.setPixelColor(i, pixels.Color(R, G, B)); pixels.show(); delay(500);}
  for(int i=15; i>0; i--) {pixels.setPixelColor(i, pixels.Color(0, 0, 0));pixels.show();delay(500);}
  off();
}
bool CheckSign(const String& str, const String& prefix) {
    if (str.length() < prefix.length()) {
        return false;
    }
    for (size_t i = 0; i < prefix.length(); ++i) {
        if (str.charAt(i) != prefix.charAt(i)) {
            return false;
        }
    }
    return true;
}
#include <WiFiEsp.h>
#include <WiFiEspServer.h>
#include <WiFiEspClient.h>
#include <ArduinoJson.h>
char ssid[] = "shtb"; char pass[] = "********"; WiFiEspServer server(8888);
void setup() {
  off();
  Serial3.begin(115200);Serial.begin(115200);Serial2.begin(9600);
  // startUpTest();
  WiFi.init(&Serial3);
  if (WiFi.status() == WL_NO_SHIELD) {Serial.println("WiFi module not detected");while (true);}
  Serial.print("Connecting to ");Serial.println(ssid);
  while (WiFi.status() != WL_CONNECTED){WiFi.begin(ssid, pass);delay(1000);}
  nexInit(); pixels.begin();
  L1.attachPop(L1CB);L2.attachPop(L2CB);L3.attachPop(L3CB);L4.attachPop(L4CB);
  L5.attachPop(L5CB);L6.attachPop(L6CB);L7.attachPop(L7CB);L8.attachPop(L8CB);
  BD1.attachPop(BD1CB);BD2.attachPop(BD2CB);BD3.attachPop(BD3CB);BD4.attachPop(BD4CB);
  BD5.attachPop(BD5CB);BD6.attachPop(BD6CB);BD7.attachPop(BD7CB);BD8.attachPop(BD8CB);
  T1.attachPop(T1CB);T2.attachPop(T2CB);T3.attachPop(T3CB);T4.attachPop(T4CB);
  T5.attachPop(T5CB);T6.attachPop(T6CB);T7.attachPop(T7CB);T8.attachPop(T8CB);
  K1.attachPop(K1CB);K2.attachPop(K2CB);K3.attachPop(K3CB);K4.attachPop(K4CB);
  K5.attachPop(K5CB);K6.attachPop(K6CB);K7.attachPop(K7CB);K8.attachPop(K8CB);
  page0.attachPush(page0PushCallback);
  page1.attachPush(page1PushCallback);
  page2.attachPush(page2PushCallback);
  page3.attachPush(page3PushCallback);
  page4.attachPush(page4PushCallback);
  Serial.print("Device IP from DHCP: ");Serial.println(WiFi.localIP());
  server.begin();Serial.println("Server started");
}
void loop() {
  WiFiEspClient client = server.available();
  if (client) {
    String jsonBuffer = "";
    bool messageStarted = false;
    while (client.connected()){
      if (client.available()){
        char c = client.read();
        if (messageStarted) {jsonBuffer += c;
          if (jsonBuffer.endsWith(END_OF_MESSAGE) || jsonBuffer.length()>900) {
            jsonBuffer.remove(0, strlen(START_OF_MESSAGE));
            jsonBuffer.remove(jsonBuffer.length() - strlen(END_OF_MESSAGE), strlen(END_OF_MESSAGE));
            Serial.print("Received message: ");
            Serial.println(jsonBuffer);
            client.println("Message received");
            messageStarted = false;
            jsonBuffer = "";
            client.stop();
            break;
          }
        }
        else {if (strncmp(&c, START_OF_MESSAGE, strlen(START_OF_MESSAGE)) == 0) {messageStarted = true;jsonBuffer = c;}}
        jsonBuffer += c;
        Serial.print(c);
      }
    }
    delay(50);
    // Serial.println("----------------------------------------------------------------------------------");
    // Serial.println(jsonBuffer);
    // Serial.println("----------------------------------------------------------------------------------");
    int unkchars = 0;
    for (int i = 0; i < jsonBuffer.length(); ++i) {
      if ((int)jsonBuffer[i]==35 && (int)jsonBuffer[i+1]==35){
        unkchars=i+2;
        break;
      }
    }
    Serial.println(unkchars);
    String strippedString = jsonBuffer.substring(unkchars);
    if (true)
    {
      int JSONlength = strippedString.length();
      DynamicJsonDocument doc(JSONlength); // Adjust the size according JSON
      DeserializationError error = deserializeJson(doc, strippedString);
      if (error) {Serial.print("deserializeJson() failed: ");Serial.println(error.c_str());}
      else {
        int index = doc["Start"]["DeviceDetails"]["DeviceID"].as<int>();
        int new_location = doc["Start"]["DataStorage"]["Location"].as<int>();
        bool old_location = devices[index].location[new_location];
        if (!old_location){
          Serial.print("ID of this device: ");Serial.println(index);
          devices[index].deviceType = doc["Start"]["DeviceDetails"]["DeviceType"].as<String>();
          devices[index].location[new_location] = true;
          devices[index].dataRetentionTime = doc["Start"]["DataStorage"]["DataRetentionTime"].as<int>();
          devices[index].locationEnabled = doc["Start"]["DataType"]["Location"].as<bool>();
          devices[index].presenceEnabled = doc["Start"]["DataType"]["Presence"].as<bool>();
          devices[index].visualEnabled = doc["Start"]["DataType"]["Visual"].as<bool>();
          devices[index].audioEnabled = doc["Start"]["DataType"]["Audio"].as<bool>();
          devices[index].biometricsEnabled = doc["Start"]["DataType"]["Biometrics"].as<bool>();
          devices[index].healthEnabled = doc["Start"]["DataType"]["Health"].as<bool>();
          devices[index].usageEnabled = doc["Start"]["DataType"]["Usage"].as<bool>();
          devices[index].environmentEnabled = doc["Start"]["DataType"]["Environment"].as<bool>();
          devices[index].resourceOwnerAccess = doc["Start"]["DataAccess"]["ResourceOwner"].as<bool>();
          devices[index].trustedPartyAccess = doc["Start"]["DataAccess"]["TrustedParty"].as<bool>();
          devices[index].serviceProviderAccess = doc["Start"]["DataAccess"]["ServiceProvider"].as<bool>();
          devices[index].deviceManufacturerAccess = doc["Start"]["DataAccess"]["DeviceManufacturer"].as<bool>();
          devices[index].lawEnforcementAccess = doc["Start"]["DataAccess"]["LawEnforcement"].as<bool>();
          devices[index].publicAccess = doc["Start"]["DataAccess"]["Public"].as<bool>();
          devices[index].thirdPartyAccess = doc["Start"]["DataAccess"]["ThirdParty"].as<bool>();
          devices[index].marketingOrganisationsAccess = doc["Start"]["DataAccess"]["MarketingOrganisations"].as<bool>();
          devices[index].revenueUsage = doc["Start"]["DataUsage"]["Revenue"].as<bool>();
          devices[index].analyticsUsage = doc["Start"]["DataUsage"]["Analytics"].as<bool>();
          devices[index].researchUsage = doc["Start"]["DataUsage"]["Research"].as<bool>();
          devices[index].surveillanceUsage = doc["Start"]["DataUsage"]["Surveillance"].as<bool>();
          devices[index].securityUsage = doc["Start"]["DataUsage"]["Security"].as<bool>();
          devices[index].targetedAdsUsage = doc["Start"]["DataUsage"]["TargetedAds"].as<bool>();
          devices[index].lifeStyleUsage = doc["Start"]["DataUsage"]["LifeStyle"].as<bool>();
          devices[index].productivityUsage = doc["Start"]["DataUsage"]["Productivity"].as<bool>();
          deviceStatus(index);
          if (index>0){SetupLEDs(index);}
        }
      }
    }
    else{
      Serial.println("Exception Handled.");
    }
    client.stop();
    Serial.println("Client disconnected");
    jsonBuffer = "";
  }
  nexLoop(nex_listen_list); // Delay a little bit to improve simulation performance
}
void deviceStatus(int dID) {
  if (dID < 0 || dID >= 40) {Serial.println("Invalid device ID");return;}
  Serial.print("Printing Report of Device ID:"); Serial.print(dID);  Serial.print(", Type:");
  Serial.print(devices[dID].deviceType);  Serial.println("<-- Locations -->"); Serial.println("1, 2, 3, 4, 5, 6, 7");
  for (int i=0; i<=6; i++){
    Serial.print(devices[dID].location[i] ? 'Y' : 'N'); Serial.print(", ");
  }
  Serial.println();Serial.print("RetTime:");
  Serial.println(devices[dID].dataRetentionTime);  Serial.print("LocEn:");
  Serial.print(devices[dID].locationEnabled ? 'Y' : 'N');  Serial.print(", PresEn:");
  Serial.print(devices[dID].presenceEnabled ? 'Y' : 'N');  Serial.print(", VisEn:");
  Serial.print(devices[dID].visualEnabled ? 'Y' : 'N');  Serial.print(", AudEn:");
  Serial.print(devices[dID].audioEnabled ? 'Y' : 'N');  Serial.print(", BioEn:");
  Serial.print(devices[dID].biometricsEnabled ? 'Y' : 'N');  Serial.print(", HealthEn:");
  Serial.print(devices[dID].healthEnabled ? 'Y' : 'N');  Serial.print(", UsageEn:");
  Serial.print(devices[dID].usageEnabled ? 'Y' : 'N');  Serial.print(", EnvEn:");
  Serial.println(devices[dID].environmentEnabled ? 'Y' : 'N');  Serial.print("ResOwnAcc:");
  Serial.print(devices[dID].resourceOwnerAccess ? 'Y' : 'N');  Serial.print(", TrustParAcc:");
  Serial.print(devices[dID].trustedPartyAccess ? 'Y' : 'N');  Serial.print(", ServProvAcc:");
  Serial.print(devices[dID].serviceProviderAccess ? 'Y' : 'N');  Serial.print(", DevManuAcc:");
  Serial.print(devices[dID].deviceManufacturerAccess ? 'Y' : 'N');  Serial.print(", LawEnfAcc:");
  Serial.print(devices[dID].lawEnforcementAccess ? 'Y' : 'N');  Serial.print(", PublicAcc:");
  Serial.print(devices[dID].publicAccess ? 'Y' : 'N');  Serial.print(", ThirdPartyAcc:");
  Serial.print(devices[dID].thirdPartyAccess ? 'Y' : 'N');  Serial.print(", MarketOrgAcc:");
  Serial.println(devices[dID].marketingOrganisationsAccess ? 'Y' : 'N');  Serial.print("RevenueUsage:");
  Serial.print(devices[dID].revenueUsage ? 'Y' : 'N');  Serial.print(", AnalyticsUsage:");
  Serial.print(devices[dID].analyticsUsage ? 'Y' : 'N');  Serial.print(", ResearchUsage:");
  Serial.print(devices[dID].researchUsage ? 'Y' : 'N');  Serial.print(", SurveillanceUsage:");
  Serial.print(devices[dID].surveillanceUsage ? 'Y' : 'N');  Serial.print(", SecurityUsage:");
  Serial.print(devices[dID].securityUsage ? 'Y' : 'N');  Serial.print(", TargetedAdsUsage:");
  Serial.print(devices[dID].targetedAdsUsage ? 'Y' : 'N');  Serial.print(", LifeStyleUsage:");
  Serial.print(devices[dID].lifeStyleUsage ? 'Y' : 'N');  Serial.print(", ProductivityUsage:");
  Serial.println(devices[dID].productivityUsage ? 'Y' : 'N');
}
// int saveDevice(const Device& data) {
//     int address = 0;
//     EEPROM.put(address, data.deviceID);address += sizeof(data.deviceID);EEPROM.put(address, data.deviceType);address += sizeof(data.deviceType);
//     EEPROM.put(address, data.location);address += sizeof(data.location);EEPROM.put(address, data.dataRetentionTime);address += sizeof(data.dataRetentionTime);
//     EEPROM.put(address, data.locationEnabled);address += sizeof(data.locationEnabled);EEPROM.put(address, data.presenceEnabled);
//     address += sizeof(data.presenceEnabled);EEPROM.put(address, data.visualEnabled);address += sizeof(data.visualEnabled);
//     EEPROM.put(address, data.audioEnabled);address += sizeof(data.audioEnabled);EEPROM.put(address, data.biometricsEnabled);address += sizeof(data.biometricsEnabled);
//     EEPROM.put(address, data.healthEnabled);address += sizeof(data.healthEnabled);EEPROM.put(address, data.usageEnabled);address += sizeof(data.usageEnabled);
//     EEPROM.put(address, data.environmentEnabled);address += sizeof(data.environmentEnabled);EEPROM.put(address, data.resourceOwnerAccess);address += sizeof(data.resourceOwnerAccess);
//     EEPROM.put(address, data.trustedPartyAccess);address += sizeof(data.trustedPartyAccess);EEPROM.put(address, data.serviceProviderAccess);address += sizeof(data.serviceProviderAccess);
//     EEPROM.put(address, data.deviceManufacturerAccess);address += sizeof(data.deviceManufacturerAccess);EEPROM.put(address, data.lawEnforcementAccess);address += sizeof(data.lawEnforcementAccess);
//     EEPROM.put(address, data.publicAccess);address += sizeof(data.publicAccess);EEPROM.put(address, data.thirdPartyAccess);address += sizeof(data.thirdPartyAccess);
//     EEPROM.put(address, data.marketingOrganisationsAccess);address += sizeof(data.marketingOrganisationsAccess);
//     EEPROM.put(address, data.revenueUsage);address += sizeof(data.revenueUsage);EEPROM.put(address, data.analyticsUsage);address += sizeof(data.analyticsUsage);
//     EEPROM.put(address, data.researchUsage);address += sizeof(data.researchUsage);EEPROM.put(address, data.surveillanceUsage);address += sizeof(data.surveillanceUsage);
//     EEPROM.put(address, data.securityUsage);address += sizeof(data.securityUsage);EEPROM.put(address, data.targetedAdsUsage);address += sizeof(data.targetedAdsUsage);
//     EEPROM.put(address, data.lifeStyleUsage);address += sizeof(data.lifeStyleUsage);EEPROM.put(address, data.productivityUsage);address += sizeof(data.productivityUsage);
//     EEPROM.commit();    Serial.print("Memory Usage: ");    Serial.println(address);
//     return address;
// }