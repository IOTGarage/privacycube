
#include "Nextion.h"
#include <ChainableLED.h>
#include <Grove_LED_Bar.h>
#include "Adafruit_NeoPixel.h"


// numpofLEDpixels, Pin, NEO_GRB + NEO_KHZ800 ( sets the bits stream to RBG at 800khz)
Adafruit_NeoPixel pixels(15, 3, NEO_GRB + NEO_KHZ800);
#define NUM_LEDS 23
ChainableLED leds(2, 3, NUM_LEDS); //clock pin, data pin, 23 LEDs, port D2

//LED bars D4,D5,D6,D7,D8,A0,A1,A2,A3

Grove_LED_Bar LocationBar(5, 4, 0); // Clock pin, Data pin, Orientation
Grove_LED_Bar PressureBar(6, 5, 0);
Grove_LED_Bar VisualBar(7, 6, 0);
Grove_LED_Bar AudioBar(8, 7, 0); 
Grove_LED_Bar BiometricBar(9, 8, 0);
Grove_LED_Bar HealthlBar(A1, A0, 0);
Grove_LED_Bar UsageBar(A2, A1, 0); 
Grove_LED_Bar EnviromentBar(A3, A2, 0);


int CurrentPage = 0;

//int m = 0; // minute
int s = 0; // second
//int t = 0; // time at which the sensor will be activated


// Create variable to store value we are going to get
//livingroom
uint32_t number0 = 0; //lock
uint32_t number1 = 0;  //vacuum
uint32_t number2 = 0; //alexa
uint32_t number3 = 0;   //camera
uint32_t number4 = 0; //tv
uint32_t number5 = 0;  //light
uint32_t number6 = 0; //thermometer
uint32_t number7 = 0;  //airourifier

//kitchen
uint32_t number8 = 0; //fridge
uint32_t number9 = 0;  //coffee
uint32_t number10 = 0; //fan
uint32_t number11 = 0;   //faucet

uint32_t number17 = 0; //kitchen tv
uint32_t number18 = 0;  //kitchen alexa
uint32_t number19 = 0; //kitchen light
uint32_t number20 = 0;   //kitchen thermo

//bedroom
uint32_t number12 = 0; //sleepmonitor

uint32_t number25 = 0;  //bedroom camera
uint32_t number26 = 0; //bedroom tv
uint32_t number27 = 0;   //bedroom alexa
uint32_t number28 = 0;   //bedroom light
uint32_t number29 = 0;  //bedroom thermo
uint32_t number30 = 0; //bedroom airpurfier
uint32_t number31 = 0;   //bedroom vacuum




//bathroom
uint32_t number13 = 0;  //mirror
uint32_t number14 = 0; //bath
uint32_t number15 = 0;   //soapdispenser
uint32_t number16 = 0;   //toothbrush

uint32_t number21 = 0;  //bthroom light
uint32_t number22 = 0; //bathroom thermo
uint32_t number23 = 0;   //bathroom fan
uint32_t number24 = 0;   //bathroom faucet



//NexDSButton anyname = NexDSButton(pageid, componentid, "objname"); // vacuum in lounge
//livingroom_devices
NexDSButton bt0l = NexDSButton(1, 3, "bt0"); // lock in livingroom 
NexDSButton bt1l = NexDSButton(1, 4, "bt1"); // vacuum in livingroom 
NexDSButton bt2l = NexDSButton(1, 10, "bt2"); // airpurifier in livingroom 
NexDSButton bt3l = NexDSButton(1, 5, "bt3"); // alexa in livingroom 
NexDSButton bt4l = NexDSButton(1, 6, "bt4"); // light in livingroom 
NexDSButton bt5l = NexDSButton(1, 7, "bt5"); // thermometer in livingroom 
NexDSButton bt6l = NexDSButton(1, 8, "bt6"); // camera in livingroom 
NexDSButton bt7l = NexDSButton(1, 9, "bt7"); // TV in livingroom 


//bedroom_devices
NexDSButton bt0b = NexDSButton(2, 6, "bt0"); // sleepmonitor in livingroom
NexDSButton bt1b = NexDSButton(2, 3, "bt1"); // vacuum in livingroom 
NexDSButton bt2b = NexDSButton(2, 4, "bt2"); // airpurifier in bedroom 
NexDSButton bt3b = NexDSButton(2, 5, "bt3"); //alexa in livingroom   
NexDSButton bt4b = NexDSButton(2, 7, "bt4"); // light in bedroom 
NexDSButton bt5b = NexDSButton(2, 8, "bt5"); // thermometer in livingroom 
NexDSButton bt6b = NexDSButton(2, 9, "bt6"); // camera in bedroom 
NexDSButton bt7b = NexDSButton(2, 10, "bt7"); // TV in bedroom 


//bathrroom_devices
NexDSButton bt0t = NexDSButton(3, 3, "bt0"); // mirror in bathroom 
NexDSButton bt1t = NexDSButton(3, 7, "bt1"); // soapdispenser in bathroom 
NexDSButton bt2t = NexDSButton(3, 5, "bt2"); // fan in bathroom 
NexDSButton bt3t = NexDSButton(3, 4, "bt3"); // bath in bathroom 
NexDSButton bt4t = NexDSButton(3, 9, "bt4"); // light in bathroom 
NexDSButton bt5t = NexDSButton(3, 10, "bt5"); // thermometer in bathroom 
NexDSButton bt6t = NexDSButton(3, 6, "bt6"); // faucet in bathroom
NexDSButton bt7t = NexDSButton(3, 8, "bt7"); // toothbrush in bathroom 

//kitchen_devices
NexDSButton bt0k = NexDSButton(4, 8, "bt0"); // coffee in kitchen 
NexDSButton bt1k = NexDSButton(4, 3, "bt1"); // fridge in kitchen 
NexDSButton bt2k = NexDSButton(4, 4, "bt2"); // fan in kitchen  
NexDSButton bt3k = NexDSButton(4, 5, "bt3"); // alexa in kitchen 
NexDSButton bt4k = NexDSButton(4, 6, "bt4"); //light in kitchen 
NexDSButton bt5k = NexDSButton(4, 7, "bt5"); // thermometer in kitchen 
NexDSButton bt6k = NexDSButton(4, 2, "bt6"); // faucet in kitchen
NexDSButton bt7k = NexDSButton(4, 9, "bt7"); // tv in kitchen

// To let arduino know what page is currently loaded, we are creating a touch event for each page.
// On the nextion project, each page most send a simulated "Touch Press Event" in the "Preinitialize Event" section so
// we can register that a new page was loaded.
NexPage page0 = NexPage(0, 0, "page0");  // Page added as a touch event (main page)
NexPage page1 = NexPage(1, 0, "page1");  // Page added as a touch event (livingroom)
NexPage page2 = NexPage(2, 0, "page2");  // Page added as a touch event (bedroom)
NexPage page3 = NexPage(3, 0, "page3");  // Page added as a touch event (bathroom)
NexPage page4 = NexPage(4, 0, "page4");  // Page added as a touch event (kitchenroom)


NexTouch *nex_listen_list[] = {
 // &b0, // Button added
 //buttons_livingroom
  &bt0l, // Dual state button added
  &bt1l, // Dual state button added
  &bt2l, // Dual state button added
  &bt3l, // Dual state button added
  &bt4l, // Dual state button added
  &bt5l, // Dual state button added
  &bt6l, // Dual state button added
  &bt7l, // Dual state button added

//bedroom_buttons
  &bt0b, // Dual state button added
  &bt1b, // Dual state button added
  &bt2b, // Dual state button added
  &bt3b, // Dual state button added
  &bt4b, // Dual state button added
  &bt5b, // Dual state button added
  &bt6b, // Dual state button added
  &bt7b, // Dual state button added


  //bathroom_buttons
  &bt0t, // Dual state button added
  &bt1t, // Dual state button added
  &bt2t, // Dual state button added
  &bt3t, // Dual state button added
  &bt4t, // Dual state button added
  &bt5t, // Dual state button added
  &bt6t, // Dual state button added
  &bt7t, // Dual state button added

  
   //kitchen_buttons
  &bt0k, // Dual state button added
  &bt1k, // Dual state button added
  &bt2k, // Dual state button added
  &bt3k, // Dual state button added
  &bt4k, // Dual state button added
  &bt5k, // Dual state button added
  &bt6k, // Dual state button added
  &bt7k, // Dual state button added
  
 
  &page0,  // Page added as a touch event
  &page1,  // Page added as a touch event
  &page2,  // Page added as a touch event
  &page3,  // Page added as a touch event
  &page4,  // Page added as a touch event
  
 
  NULL // String terminated
};



void off(){
  
  //turn off all
    LocationBar.setBits(0x0);
    PressureBar.setBits(0x0);
    VisualBar.setBits(0x0);
    AudioBar.setBits(0x0);
    BiometricBar.setBits(0x0);
    HealthlBar.setBits(0x0);
    UsageBar.setBits(0x0);
    EnviromentBar.setBits(0x0);

leds.setColorRGB(0, 0, 0, 0);leds.setColorRGB(1, 0, 0, 0);leds.setColorRGB(2, 0, 0, 0);leds.setColorRGB(3, 0, 0, 0);
leds.setColorRGB(4, 0, 0, 0);leds.setColorRGB(5, 0, 0, 0);leds.setColorRGB(6, 0, 0, 0);leds.setColorRGB(7, 0, 0, 0);
leds.setColorRGB(8, 0, 0, 0);leds.setColorRGB(9, 0, 0, 0);leds.setColorRGB(10, 0, 0, 0);leds.setColorRGB(11, 0, 0, 0);
leds.setColorRGB(12, 0, 0, 0);leds.setColorRGB(13, 0, 0, 0);leds.setColorRGB(14, 0, 0, 0);leds.setColorRGB(15, 0, 0, 0);
leds.setColorRGB(16, 0, 0, 0);leds.setColorRGB(17, 0, 0, 0);leds.setColorRGB(18, 0, 0, 0);leds.setColorRGB(19, 0, 0, 0);
leds.setColorRGB(20, 0, 0, 0);leds.setColorRGB(21, 0, 0, 0);leds.setColorRGB(22, 0, 0, 0);


  for(int i=0; i<15; i++) { // For each pixel...
    pixels.setPixelColor(i, pixels.Color(0, 0, 0));
    pixels.show();   // Send the updated pixel colors to the hardware.
  }
  }

  void btoff (){
    
    number1 == 0; // smart vacuum off
    number3 == 0; //camera off
    number0 == 0; //smart lock off
    number6 == 0; //thermo off
    number5 == 0; // light off
    number4 == 0; // tv off
    number7 == 0; // purifier off
    
    number8 == 0; //fridge off 
    number9 == 0; // coffee off
    number10 == 0; // fan off
    number11 == 0; // faucet off
    
    number12 == 0; // monitor on

    number13 == 0; // mirror on
    number14 == 0; // bath on
    number15 == 0; // soapdispenser on
    number16 == 0; // toothbrush on
    
    
    bt1l.setValue(0); // smart vacuum off
    bt3l.setValue(0); //smart speaker off
    bt6l.setValue(0); // camera off
    bt0l.setValue(0); // lock is off
    bt5l.setValue(0); // thermo is off
    bt4l.setValue(0); // light is off
    bt7l.setValue(0); // tv is off
    bt2l.setValue(0); // purifier is off
    
    bt1k.setValue(0); // fridge is on
    bt0k.setValue(0); // coffee on 
    bt2k.setValue(0); // fan on 
    bt6k.setValue(0); // faucet off

    bt0b.setValue(0); // sleepmonitor on

    bt0t.setValue(0); // mirror on
    bt3t.setValue(0); // bath on
    bt1t.setValue(0); // soapdispenser on
    bt7t.setValue(0); // toothbrush on
    
    }

    void yellow(){
// rgb for smart speaker, face B
leds.setColorRGB(7, 255, 255, 0); // red for device manufacturer 
leds.setColorRGB(8, 255, 255, 0); // yellow service provider 
leds.setColorRGB(9, 255, 255, 0); // yellow trusted party 
leds.setColorRGB(10, 0, 204, 0); // green for Resource owner
leds.setColorRGB(11, 255, 255, 0); // red for mareting orgnization 
leds.setColorRGB(12, 255, 255, 0); // red for third party
leds.setColorRGB(14, 255, 255, 0); // red for law enforcemnt

// rgb for smart speaker, face R

leds.setColorRGB(16,255, 255, 0); // yellow for research 
leds.setColorRGB(17,255, 255, 0); // red for Anaytics
leds.setColorRGB(18,255, 255, 0); // yellow for revenue 
leds.setColorRGB(19,255, 255, 0); // yellow for Productivity 
leds.setColorRGB(20,255, 255, 0); // yellow for Lifestyle 
leds.setColorRGB(21,255, 255, 0); // red for Targeted Ads

   //data retention 
   
for(int i=0; i<15; i++) { // For each pixel...

    // pixels.Color() takes RGB values, from 0,0,0 up to 255,255,255
    // Here we're using a moderately bright green color:
    pixels.setPixelColor(i, pixels.Color(255, 255, 0));

    pixels.show();   // Send the updated pixel colors to the hardware.

  }     
    }


  void  red (){
      // rgb for smart lock, face B

leds.setColorRGB(8, 255, 255, 0); // yellow service provider
leds.setColorRGB(9, 255, 255, 0); // yellow trusted party
leds.setColorRGB(10, 0, 204, 0); // green for Resource owner
leds.setColorRGB(7, 255, 0, 0); // red for device manufacturer
leds.setColorRGB(14, 255, 0, 0); // red for law enforcemnt
leds.setColorRGB(12, 255, 0, 0); // red for third party
leds.setColorRGB(11, 255, 0, 0); // red for mareting orgnization

// rgb for smart fridge, face R
leds.setColorRGB(18,255, 0, 0); // yellow for revenue
leds.setColorRGB(17,255, 0, 0); // red for Anaytics
leds.setColorRGB(16,255, 0, 0); // red for research
leds.setColorRGB(21,255, 0, 0); // red for Targeted Ads
leds.setColorRGB(20,255, 0, 0); // red for Lifestyle
leds.setColorRGB(19,255, 0, 0); // red for Productivity

   //data retention
   
for(int i=0; i<15; i++) { // For each pixel...
    pixels.setPixelColor(i, pixels.Color(255, 0, 0));
    pixels.show();   // Send the updated pixel colors to the hardware.

  }
    }

void light(){//turn on ledbars for the smart light
PressureBar.setBits(0b000000000011111);
EnviromentBar.setBits(0b000000000011111); //only the second from bottom is on to indicate that the swcond device is collecting data
UsageBar.setBits(0b000000000011111);

//face l 
leds.setColorRGB(0, 255, 255, 0); // red for north america
leds.setColorRGB(1, 255, 255, 0); // red for Europe
leds.setColorRGB(2, 255, 255, 0); // red for north america
leds.setColorRGB(3, 255, 255, 0); // red for Europe
leds.setColorRGB(4, 255, 255, 0); // red for north america
leds.setColorRGB(5, 255, 255, 0); // red for Europe
leds.setColorRGB(6, 255, 255, 0); // red for north america



// rgb for smart lock, face B
leds.setColorRGB(8, 255, 255, 0); // yellow service provider 
leds.setColorRGB(9, 255, 255, 0); // yellow trusted party 
leds.setColorRGB(10, 0, 204, 0); // green for Resource owner
leds.setColorRGB(14, 255, 255, 0); // red for law enforcemnt

// rgb for smart lock, face R
leds.setColorRGB(18,255, 255, 0); // yellow for revenue
leds.setColorRGB(17,255, 255, 0); // red for Anaytics
leds.setColorRGB(21,255, 255, 0); // red for Targeted Ads
leds.setColorRGB(20,255, 255, 0); // red for Lifestyle
leds.setColorRGB(19,255, 255, 0); // red for Productivity




   //data retention 
   
for(int i=0; i<15; i++) { // For each pixel...

    // pixels.Color() takes RGB values, from 0,0,0 up to 255,255,255
    // Here we're using a moderately bright green color:
    pixels.setPixelColor(i, pixels.Color(255, 255, 0));
    pixels.show();   // Send the updated pixel colors to the hardware.

  }}

    void thermo (){//turn on ledbars for the smart thermo
PressureBar.setBits(0b000000000111111); //only the second from bottom is on to indicate that the swcond device is collecting data
 EnviromentBar.setBits(0b000000000111111);
UsageBar.setBits(0b000000000111111);

//face l 
leds.setColorRGB(1, 255, 255, 0); // yellow for asia
leds.setColorRGB(3, 255, 255, 0); // yellow for Europe
leds.setColorRGB(5, 255, 255, 0); // yellow for north america
leds.setColorRGB(6, 255, 255, 0); // yellow for South america

// rgb for smart thermo, face B
leds.setColorRGB(8, 255, 255, 0); // yellow service provider 
leds.setColorRGB(9, 255, 255, 0); // yellow trusted party 
leds.setColorRGB(10, 0, 204, 0); // green for Resource owner
leds.setColorRGB(14, 255, 255, 0); // yellow for law enforcemnt

// rgb for smart thermo, face R
leds.setColorRGB(18,255, 255, 0); // red for revenue
leds.setColorRGB(17,255, 255, 0); // red for Anaytics
leds.setColorRGB(20,255, 255, 0); // red for Lifestyle
leds.setColorRGB(19,255, 255, 0); // red for Productivity

   //data retention   
for(int i=0; i<10; i++) { // For each pixel...

    // pixels.Color() takes RGB values, from 0,0,0 up to 255,255,255
    // Here we're using a moderately bright green color:
    pixels.setPixelColor(i, pixels.Color(255, 255, 0));
    pixels.show();   // Send the updated pixel colors to the hardware.

  }
  }


  

 void camera () {
PressureBar.setBits(0b000000000000001); //only the second from bottom is on to indicate that the swcond device is collecting data
BiometricBar.setBits(0b000000000000001);
AudioBar.setBits(0b000000000000001);
VisualBar.setBits(0b000000000000001);  
UsageBar.setBits(0b000000000000001);

//face l 
leds.setColorRGB(1, 255, 0, 0); // red for asia
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(5, 255, 0, 0); // red for north america
leds.setColorRGB(6, 255, 0, 0); // red for South america

// rgb for smart lock, face B
leds.setColorRGB(8, 255, 255, 0); // yellow service provider 
leds.setColorRGB(9, 255, 255, 0); // yellow trusted party 
leds.setColorRGB(10, 0, 204, 0); // green for Resource owner
leds.setColorRGB(14, 255, 0, 0); // red for law enforcemnt
leds.setColorRGB(12, 255, 0, 0); // red for third party
leds.setColorRGB(11, 255, 0, 0); // red for mareting orgnization

// rgb for smart lock, face R
leds.setColorRGB(18,255, 0, 0); // red for revenue
leds.setColorRGB(17,255, 0, 0); // red for Anaytics
leds.setColorRGB(16,255, 0, 0); // red for research
leds.setColorRGB(15,255, 0, 0); // red for survellance
leds.setColorRGB(22,255, 0, 0); // red for security
leds.setColorRGB(21,255, 0, 0); // red for Targeted Ads
leds.setColorRGB(20,255, 0, 0); // red for Lifestyle
leds.setColorRGB(19,255, 0, 0); // red for Productivity




   //data retention   
for(int i=0; i<10; i++) { // For each pixel...

    // pixels.Color() takes RGB values, from 0,0,0 up to 255,255,255
    // Here we're using a moderately bright green color:
    pixels.setPixelColor(i, pixels.Color(255, 0, 0));
    pixels.show();   // Send the updated pixel colors to the hardware.

  }}
////////////////////////// ////////////////////////////////////////////////////////////////////////////////Touch events:
//livingroom
//smart lock
void bt0lPopCallback(void *ptr) {
   // Create variable to store value we are going to get
bt0l.getValue(&number0);  // Read value of dual state button to know the state (0 or 1)

if(number0 == 1){  // If dual state button is equal to 1 (meaning is ON)...

//turn on ledbars for the smart lock
EnviromentBar.setBits(0b000000000000011); //only the second from bottom is on to indicate that the swcond device is collecting data
BiometricBar.setBits(0b000000000000011);
AudioBar.setBits(0b000000000000011);
LocationBar.setBits(0b000000000000011);
VisualBar.setBits(0b000000000000011);  
UsageBar.setBits(0b000000000000011);

//face l 
leds.setColorRGB(6, 255, 0, 0); // red for north america
leds.setColorRGB(3, 255, 0, 0); // red for Europe

red ();

leds.setColorRGB(15,255, 0, 0); // yellow for survellance
leds.setColorRGB(22,255, 0, 0); // red for security
 
   
  
  }
  else{  off();

  }

}

//livingroom
//smart speaker     
void bt3lPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt3l.getValue(&number2);  // Read value of dual state button to know the state (0 or 1)

  if(number2 == 1){  // If dual state button is equal to 1 (meaning is ON)...


//turn on ledbars for the smart speaker
AudioBar.setBits(0b000000000001111);
LocationBar.setBits(0b000000000001111); 
UsageBar.setBits(0b000000000001111);



//face l 
leds.setColorRGB(0, 255, 0, 0); // red for asia
leds.setColorRGB(1, 255, 0, 0); // red for autralia
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(5, 255, 0, 0); // red for southamerica
leds.setColorRGB(6, 255, 0, 0); // red for unitedstates


// rgb for smart speaker, face B

red ();

leds.setColorRGB(7, 0, 0, 0); // off for device manufacturere 
leds.setColorRGB(16,0, 0, 0); // off for research
  }
  else{  off();

  }

}

 

//vacuum in livingroom
void bt1lPopCallback(void *ptr) {
  bt1l.getValue(&number1);  // Read value of dual state button to know the state (0 or 1)

  if(number1 == 1){  // If dual state button is equal to 1 (meaning is ON)...

   PressureBar.setBits(0b000000011111111);
   EnviromentBar.setBits(0b000000011111111);
   UsageBar.setBits(0b000000011111111);
    
leds.setColorRGB(1, 255, 255, 0); // yellow for asia
leds.setColorRGB(3, 255, 255, 0); // red for Europe
leds.setColorRGB(6, 255, 255, 0); // yellow for unitedstates

yellow();
  
  }
  else{  // Since the dual state button is OFF...

   off();

  }

}

//livingroom
//smart camera
void bt6lPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt6l.getValue(&number3);  // Read value of dual state button to know the state (0 or 1)

  if(number3 == 1){  // If dual state button is equal to 1 (meaning is ON)...


camera ();
  
  }
  else{  off();

  }

}

//livingroom
//smart thermometer
void bt5lPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt5l.getValue(&number6);  // Read value of dual state button to know the state (0 or 1)

  if(number6 == 1){  // If dual state button is equal to 1 (meaning is ON)...


thermo ();
  }
  else{  off();
  }

}

//livingroom
//smart light
void bt4lPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt4l.getValue(&number5);  // Read value of dual state button to know the state (0 or 1)

  if(number5 == 1){  // If dual state button is equal to 1 (meaning is ON)...


light ();
  
  }
  else{  off();

  }

}


//livingroom
//smart tv
void bt7lPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt7l.getValue(&number4);  // Read value of dual state button to know the state (0 or 1)

  if(number4 == 1){  // If dual state button is equal to 1 (meaning is ON)...
BiometricBar.setBits(0b000000000000111);
AudioBar.setBits(0b000000000000111);
LocationBar.setBits(0b000000000000111);
VisualBar.setBits(0b000000000000111);  
UsageBar.setBits(0b000000000000111);

//face l 
leds.setColorRGB(1, 255, 0, 0); // red for north america
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(5, 255, 0, 0); // red for north america
leds.setColorRGB(6, 255, 0, 0); // red for Europe

red ();
  
  }
  else{  off();

  }

}


//livingroom
//smart air purifier
void bt2lPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt2l.getValue(&number7);  // Read value of dual state button to know the state (0 or 1)

  if(number7 == 1){  // If dual state button is equal to 1 (meaning is ON)...


//turn on ledbars for the smart lock
PressureBar.setBits(0b000000001111111);
EnviromentBar.setBits(0b000000001111111); //only the second from bottom is on to indicate that the swcond device is collecting data
LocationBar.setBits(0b000000001111111); 
UsageBar.setBits(0b000000001111111);

//face l 
leds.setColorRGB(0, 255, 255, 0); // red for north america
leds.setColorRGB(1, 255, 255, 0); // red for Europe
leds.setColorRGB(2, 255, 255, 0); // red for north america
leds.setColorRGB(3, 255, 255, 0); // red for Europe
leds.setColorRGB(4, 255, 255, 0); // red for north america
leds.setColorRGB(5, 255, 255, 0); // red for Europe
leds.setColorRGB(6, 255, 255, 0); // red for north america



yellow();
  
  }
  else{  off();

  }

}



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//kitchen
//smart fridge
void bt1kPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt1k.getValue(&number8);  // Read value of dual state button to know the state (0 or 1)

  if(number8 == 1){  // If dual state button is equal to 1 (meaning is ON)...


//turn on ledbars for the smart lock
EnviromentBar.setBits(0b000000000000001); 
VisualBar.setBits(0b000000000000001);
LocationBar.setBits(0b000000000000001);
HealthlBar.setBits(0b000000000000001);  
UsageBar.setBits(0b000000000000001);

//face l 
leds.setColorRGB(1, 255, 0, 0); // red for north america
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(5, 255, 0, 0); // red for north america
leds.setColorRGB(6, 255, 0, 0); // red for Europe

red ();
  
  }
  else{  off();

  }

}



//kitchen
//smart coffee
void bt0kPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt0k.getValue(&number9);  // Read value of dual state button to know the state (0 or 1)

  if(number9 == 1){  // If dual state button is equal to 1 (meaning is ON)...


//turn on ledbars for the smart lock
 
UsageBar.setBits(0b000000000000011);

//face l 

leds.setColorRGB(3, 0, 204, 0); // green for Europe
leds.setColorRGB(1, 0, 204, 0);// greem for  asia



// rgb for smart lock, face B
leds.setColorRGB(8, 0, 204, 0); // yellow service provider 
leds.setColorRGB(9, 0, 204, 0); // yellow trusted party 
leds.setColorRGB(10, 0, 204, 0); // green for Resource owner
leds.setColorRGB(7, 0, 204, 0); // red for device manufacturer
leds.setColorRGB(14, 255, 255, 0); // red for law enforcemnt
leds.setColorRGB(12, 0, 204, 0); // red for third party
leds.setColorRGB(11, 0, 204, 0); // red for mareting orgnization



// rgb for smart fridge, face R
leds.setColorRGB(18,0, 204, 0); // yellow for revenue
leds.setColorRGB(17,0, 204, 0); // red for Anaytics
leds.setColorRGB(16,0, 204, 0); // red for research
leds.setColorRGB(21,0, 204, 0); // red for Targeted Ads
leds.setColorRGB(20,0, 204, 0); // red for Lifestyle
leds.setColorRGB(19,0, 204, 0); // red for Productivity




   //data retention 
   
for(int i=0; i<15; i++) { // For each pixel...
    pixels.setPixelColor(i, pixels.Color(0, 204, 0));
    pixels.show();   // Send the updated pixel colors to the hardware.

  }
  
  }
  else{  off();

  }

}

//fan in kitchen
void bt2kPopCallback(void *ptr) {
  bt2k.getValue(&number10);  // Read value of dual state button to know the state (0 or 1)
 
  if(number10 == 1){  // If dual state button is equal to 1 (meaning is ON)...

   LocationBar.setBits(0b000000001111111);
   EnviromentBar.setBits(0b000000001111111);
   UsageBar.setBits(0b000000001111111);
    
leds.setColorRGB(0, 255, 255, 0); // yellow for asia
leds.setColorRGB(1, 255, 255, 0); // red for Europe
leds.setColorRGB(2, 255, 255, 0); // yellow for unitedstates
leds.setColorRGB(3, 255, 255, 0); // yellow for asia
leds.setColorRGB(4, 255, 255, 0); // red for Europe
leds.setColorRGB(5, 255, 255, 0); // yellow for unitedstates
leds.setColorRGB(6, 255, 255, 0); // yellow for asia

yellow();
  
  }
  else{ off();

  }

}



//kitchen
//smart faucet
void bt6kPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt6k.getValue(&number11);  // Read value of dual state button to know the state (0 or 1)

  if(number11 == 1){  // If dual state button is equal to 1 (meaning is ON)...
//turn on ledbars for the smart speaker
AudioBar.setBits(0b000000011111111);
LocationBar.setBits(0b000000011111111); 
UsageBar.setBits(0b000000011111111);
PressureBar.setBits(0b000000011111111);



//face l 

leds.setColorRGB(0, 255, 0, 0); // red for asia
leds.setColorRGB(1, 255, 0, 0); // red for autralia
leds.setColorRGB(2, 255, 0, 0); // red for autralia
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(4, 255, 0, 0); // red for autralia
leds.setColorRGB(5, 255, 0, 0); // red for southamerica
leds.setColorRGB(6, 255, 0, 0); // red for unitedstates

red();
  }
  else{  off();

  }

}



//kitchen
//smart tv
void bt7kPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt7k.getValue(&number17);  // Read value of dual state button to know the state (0 or 1)

  if(number17 == 1){  // If dual state button is equal to 1 (meaning is ON)...


//turn on ledbars for the smart lock
BiometricBar.setBits(0b000000000000111);
AudioBar.setBits(0b000000000000111);
LocationBar.setBits(0b000000000000111);
VisualBar.setBits(0b000000000000111);  
UsageBar.setBits(0b000000000000111);

//face l 
leds.setColorRGB(1, 255, 0, 0); // red for north america
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(5, 255, 0, 0); // red for north america
leds.setColorRGB(6, 255, 0, 0); // red for Europe

red ();
  
  }
  else{  off();

  }

}


//kitchen
//smart alexa
void bt3kPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt3k.getValue(&number18);  // Read value of dual state button to know the state (0 or 1)

  if(number18 == 1){  // If dual state button is equal to 1 (meaning is ON)...

//turn on ledbars for the smart speaker
AudioBar.setBits(0b000000000001111);
LocationBar.setBits(0b000000000001111); 
UsageBar.setBits(0b000000000001111);



//face l 
leds.setColorRGB(0, 255, 0, 0); // red for asia
leds.setColorRGB(1, 255, 0, 0); // red for autralia
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(5, 255, 0, 0); // red for southamerica
leds.setColorRGB(6, 255, 0, 0); // red for unitedstates


// rgb for smart speaker, face B


red ();

leds.setColorRGB(7, 0, 0, 0); // off for device manufacturere 
leds.setColorRGB(16,0, 0, 0); // off for research

  
  }
  else{  off();

  }

}


//kitchen
//smart light
void bt4kPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt4k.getValue(&number19);  // Read value of dual state button to know the state (0 or 1)

  if(number19 == 1){  // If dual state button is equal to 1 (meaning is ON)...

light ();
  
  }
  else{  off();

  }

}


//kitchen
//smart thermo
void bt5kPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt5k.getValue(&number20);  // Read value of dual state button to know the state (0 or 1)

  if(number20 == 1){  // If dual state button is equal to 1 (meaning is ON)...


thermo ();
  
  }
  else{  off();

  }

}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//bedroom
//smart sleepmonitor
void bt0bPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt0b.getValue(&number12);  // Read value of dual state button to know the state (0 or 1)

  if(number12 == 1){  // If dual state button is equal to 1 (meaning is ON)...


//turn on ledbars for the smart lock

PressureBar.setBits(0b000000000000011);
AudioBar.setBits(0b000000000000011);
BiometricBar.setBits(0b000000000000011);
HealthlBar.setBits(0b000000000000011);  
UsageBar.setBits(0b000000000000011);

//face l
leds.setColorRGB(0, 255, 0, 0); // red for north america
leds.setColorRGB(1, 255, 0, 0); // red for Europe
leds.setColorRGB(2, 255, 0, 0); // red for north america
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(4, 255, 0, 0); // red for north america
leds.setColorRGB(5, 255, 0, 0); // red for Europe
leds.setColorRGB(6, 255, 0, 0); // red for north america

red();
 
  }
  else{  off();

  }

}






//bedroom
//smart speaker     
void bt3bPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt3b.getValue(&number27);  // Read value of dual state button to know the state (0 or 1)

  if(number27 == 1){  // If dual state button is equal to 1 (meaning is ON)...


//turn on ledbars for the smart speaker
AudioBar.setBits(0b000000000001111);
LocationBar.setBits(0b000000000001111); 
UsageBar.setBits(0b000000000001111);



//face l 
leds.setColorRGB(0, 255, 0, 0); // red for asia
leds.setColorRGB(1, 255, 0, 0); // red for autralia
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(5, 255, 0, 0); // red for southamerica
leds.setColorRGB(6, 255, 0, 0); // red for unitedstates


// rgb for smart speaker, face B



red ();

leds.setColorRGB(7, 0, 0, 0); // off for device manufacturere 
leds.setColorRGB(16,0, 0, 0); // off for research
  }
  else{  off();

  }

}

 
//bedroom
//smart vacuum
void bt1bPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt1b.getValue(&number31);  // Read value of dual state button to know the state (0 or 1)

  if(number31 == 1){  // If dual state button is equal to 1 (meaning is ON)...

   PressureBar.setBits(0b000000011111111);
   EnviromentBar.setBits(0b000000011111111);
   UsageBar.setBits(0b000000011111111);
    
leds.setColorRGB(1, 255, 255, 0); // yellow for asia
leds.setColorRGB(3, 255, 255, 0); // red for Europe
leds.setColorRGB(6, 255, 255, 0); // yellow for unitedstates

yellow();
  
  }
  else{  // Since the dual state button is OFF...

   off();

  }

}

//bedroom
//smart camera
void bt6bPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt6b.getValue(&number25);  // Read value of dual state button to know the state (0 or 1)

  if(number25 == 1){  // If dual state button is equal to 1 (meaning is ON)...


camera ();
  
  }
  else{  off();

  }

}

//bedroom
//smart thermometer
void bt5bPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt5b.getValue(&number29);  // Read value of dual state button to know the state (0 or 1)

  if(number29 == 1){  // If dual state button is equal to 1 (meaning is ON)...


thermo ();
  }
  else{  off();
  }

}

//bedroom
//smart light
void bt4bPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt4b.getValue(&number28);  // Read value of dual state button to know the state (0 or 1)

  if(number28 == 1){  // If dual state button is equal to 1 (meaning is ON)...


light ();
  
  }
  else{  off();

  }

}


//bedroom
//smart tv
void bt7bPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt7b.getValue(&number26);  // Read value of dual state button to know the state (0 or 1)

  if(number26 == 1){  // If dual state button is equal to 1 (meaning is ON)...
BiometricBar.setBits(0b000000000000111);
AudioBar.setBits(0b000000000000111);
LocationBar.setBits(0b000000000000111);
VisualBar.setBits(0b000000000000111);  
UsageBar.setBits(0b000000000000111);

//face l 
leds.setColorRGB(1, 255, 0, 0); // red for north america
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(5, 255, 0, 0); // red for north america
leds.setColorRGB(6, 255, 0, 0); // red for Europe

red ();
  
  }
  else{  off();

  }

}


//bedroom
//smart air purifier
void bt2bPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt2b.getValue(&number30);  // Read value of dual state button to know the state (0 or 1)

  if(number30 == 1){  // If dual state button is equal to 1 (meaning is ON)...


//turn on ledbars for the smart lock
PressureBar.setBits(0b000000001111111);
EnviromentBar.setBits(0b000000001111111); //only the second from bottom is on to indicate that the swcond device is collecting data
LocationBar.setBits(0b000000001111111); 
UsageBar.setBits(0b000000001111111);

//face l 
leds.setColorRGB(0, 255, 255, 0); // red for north america
leds.setColorRGB(1, 255, 255, 0); // red for Europe
leds.setColorRGB(2, 255, 255, 0); // red for north america
leds.setColorRGB(3, 255, 255, 0); // red for Europe
leds.setColorRGB(4, 255, 255, 0); // red for north america
leds.setColorRGB(5, 255, 255, 0); // red for Europe
leds.setColorRGB(6, 255, 255, 0); // red for north america



yellow();
  
  }
  else{  off();

  }

}



///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//bathroom
//smart mirror
void bt0tPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt0t.getValue(&number13);  // Read value of dual state button to know the state (0 or 1)

  if(number13 == 1){  // If dual state button is equal to 1 (meaning is ON)...


//turn on ledbars for the smart lock

PressureBar.setBits(0b000000000000001);
BiometricBar.setBits(0b000000000000001);
UsageBar.setBits(0b000000000000001);
EnviromentBar.setBits(0b000000000000001);

//face l

leds.setColorRGB(3, 255, 255, 0); // red for Europe
leds.setColorRGB(6, 255, 255, 0); // red for north america

yellow();

  }
  else{ off();

  }

}



//bathroom
//smart bath
void bt3tPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt3t.getValue(&number14);  // Read value of dual state button to know the state (0 or 1)

  if(number14 == 1){  // If dual state button is equal to 1 (meaning is ON)...


//turn on ledbars for the smart lock

PressureBar.setBits(0b000000000001111);
//BiometricBar.setBits(0b000000000001111);
UsageBar.setBits(0b000000000001111);
EnviromentBar.setBits(0b000000000001111);

//face l

leds.setColorRGB(0, 255, 255, 0); // red for Europe
leds.setColorRGB(1, 255, 255, 0); // red for north america
leds.setColorRGB(2, 255, 255, 0); // red for Europe
leds.setColorRGB(3, 255, 255, 0); // red for north america
leds.setColorRGB(4, 255, 255, 0); // red for Europe
leds.setColorRGB(5, 255, 255, 0); // red for north america
leds.setColorRGB(6, 255, 255, 0); // red for Europe

yellow();

 
  }
  else{  off();

  }

}

//bathroom
//smart soapdispenser
void bt1tPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt1t.getValue(&number15);  // Read value of dual state button to know the state (0 or 1)

  if(number15 == 1){  // If dual state button is equal to 1 (meaning is ON)...


//turn on ledbars for the smart lock
PressureBar.setBits(0b000000000000011);
UsageBar.setBits(0b000000000000011);

//face l 

leds.setColorRGB(0, 255, 255, 0); // red for asia
leds.setColorRGB(1, 255, 255, 0); // red for autralia
leds.setColorRGB(3, 255, 255, 0); // red for Europe
leds.setColorRGB(5, 255, 255, 0); // red for southamerica
leds.setColorRGB(6, 255, 255, 0); // red for unitedstates



yellow ();

leds.setColorRGB(7, 0, 0, 0); // off for device manufacturere 
leds.setColorRGB(16,0, 0, 0); // off for research
  }
  else{  // Since the dual state button is OFF...
   //turn off all bars
  off();

  }

}


//bathroom
//smart toothbrush
void bt7tPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt7t.getValue(&number16);  // Read value of dual state button to know the state (0 or 1)

  if(number16 == 1){  // If dual state button is equal to 1 (meaning is ON)...

//turn on ledbars for the smart toothbrush
PressureBar.setBits(0b000000000000111);
UsageBar.setBits(0b000000000000111);
BiometricBar.setBits(0b000000000000111);
HealthlBar.setBits(0b000000000000111);
    
//face l 
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(6, 255, 0, 0); // red for unitedstates

red ();
  }
  else{  
    
    // Since the dual state button is OFF...
off();

  }

}



//bathroom
//smart light
void bt4tPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt4t.getValue(&number21);  // Read value of dual state button to know the state (0 or 1)

  if(number21 == 1){  // If dual state button is equal to 1 (meaning is ON)...


light ();

  }
  else{ off();

  }

}


//bathroom
//smart thermo
void bt5tPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt5t.getValue(&number22);  // Read value of dual state button to know the state (0 or 1)

  if(number22 == 1){  // If dual state button is equal to 1 (meaning is ON)...


thermo ();

  }
  else{ off();

  }

}


//bathroom
//smart fan
void bt2tPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt2t.getValue(&number23);  // Read value of dual state button to know the state (0 or 1)

  if(number23 == 1){  // If dual state button is equal to 1 (meaning is ON)...
LocationBar.setBits(0b000000001111111);
   EnviromentBar.setBits(0b000000001111111);
   UsageBar.setBits(0b000000001111111);
    
leds.setColorRGB(0, 255, 255, 0); // yellow for asia
leds.setColorRGB(1, 255, 255, 0); // red for Europe
leds.setColorRGB(2, 255, 255, 0); // yellow for unitedstates
leds.setColorRGB(3, 255, 255, 0); // yellow for asia
leds.setColorRGB(4, 255, 255, 0); // red for Europe
leds.setColorRGB(5, 255, 255, 0); // yellow for unitedstates
leds.setColorRGB(6, 255, 255, 0); // yellow for asia

yellow();

  }
  else{ off();

  }

}


//bathroom
//smart faucet
void bt6tPopCallback(void *ptr) {
   // Create variable to store value we are going to get
  bt6t.getValue(&number24);  // Read value of dual state button to know the state (0 or 1)

  if(number24 == 1){  // If dual state button is equal to 1 (meaning is ON)...

AudioBar.setBits(0b000000011111111);
LocationBar.setBits(0b000000011111111); 
UsageBar.setBits(0b000000011111111);
PressureBar.setBits(0b000000011111111);



//face l 

leds.setColorRGB(0, 255, 0, 0); // red for asia
leds.setColorRGB(1, 255, 0, 0); // red for autralia
leds.setColorRGB(2, 255, 0, 0); // red for autralia
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(4, 255, 0, 0); // red for autralia
leds.setColorRGB(5, 255, 0, 0); // red for southamerica
leds.setColorRGB(6, 255, 0, 0); // red for unitedstates

red();

  }
  else{ off();

  }

}



// Page change event:
void page0PushCallback(void *ptr)  // If page 0 is loaded on the display, the following is going to execute:
{
  CurrentPage = 0;  // Set variable as 0 so from now on arduino knows page 0 is loaded on the display
  //refreshScreen = true;

}  // End of press event

void page1PushCallback(void *ptr)  // If page 1 is loaded on the display, the following is going to execute:
{
  CurrentPage = 1;  // Set variable to 1 so from now on arduino knows page 1 is loaded on the display
 // refreshScreen = true;
}  // End of press event


void page2PushCallback(void *ptr)  // If page 2 is loaded on the display, the following is going to execute:
{
  CurrentPage = 2;  // Set variable to 2 so from now on arduino knows page 2 is loaded on the display
 // refreshScreen = true;
 
}  // End of press event


void page3PushCallback(void *ptr)  // If page 3 is loaded on the display, the following is going to execute:
{
  CurrentPage = 3;  // Set variable to 3 so from now on arduino knows page 3 is loaded on the display
 // refreshScreen = true;
 
}  // End of press event

// Page change event:
void page4PushCallback(void *ptr)  // If page 4 is loaded on the display, the following is going to execute:
{
  CurrentPage = 4;  // Set variable to 4 so from now on arduino knows page 4 is loaded on the display
 // refreshScreen = true;
 
}  // End of press event

void setup()
{
 
 //  leds.init(); //initialise LEDs
  Serial1.begin(9600);


  Serial1.println("on");  // Set new baud rate of nextion to 115200, but it's temporal. Next time nextion is power on,
                                // it will retore to default baud of 9600.



   nexInit();
   pixels.begin(); // initilize led strip
   off();

  

 //b0.attachPop(bt1PopCallback, &b0);
 //livingroom touch
bt0l.attachPop(bt0lPopCallback);
bt1l.attachPop(bt1lPopCallback);
bt2l.attachPop(bt2lPopCallback);
bt3l.attachPop(bt3lPopCallback);
bt4l.attachPop(bt4lPopCallback);
bt5l.attachPop(bt5lPopCallback);
bt6l.attachPop(bt6lPopCallback);
bt7l.attachPop(bt7lPopCallback);



//kitchen touch
bt1k.attachPop(bt1kPopCallback); //fridge
bt0k.attachPop(bt0kPopCallback); //coffee
bt2k.attachPop(bt2kPopCallback); //fan
bt6k.attachPop(bt6kPopCallback); //faucet

bt7k.attachPop(bt7kPopCallback); //tv
bt3k.attachPop(bt3kPopCallback); //alexa
bt5k.attachPop(bt5kPopCallback); //themor
bt4k.attachPop(bt4kPopCallback); //light


//bedroom touch
bt0b.attachPop(bt0bPopCallback); //sleep

bt1b.attachPop(bt1bPopCallback); //vacuum
bt2b.attachPop(bt2bPopCallback); //airpurifier
bt3b.attachPop(bt3bPopCallback); //alexa
bt4b.attachPop(bt4bPopCallback); //ligh
bt5b.attachPop(bt5bPopCallback); //thermo
bt6b.attachPop(bt6bPopCallback); //camera
bt7b.attachPop(bt7bPopCallback); //tv

////////////////////////////////////////////////////////////////////////
//bathroom touch
bt1t.attachPop(bt1tPopCallback); //soap
bt0t.attachPop(bt0tPopCallback); //mirror
bt7t.attachPop(bt7tPopCallback); //toothbrush
bt3t.attachPop(bt3tPopCallback); //bath

bt4t.attachPop(bt4tPopCallback); //light
bt5t.attachPop(bt5tPopCallback); //thermo
bt2t.attachPop(bt2tPopCallback); //fan
bt6t.attachPop(bt6tPopCallback); //faucet




page0.attachPush(page0PushCallback);  // Page press event
page1.attachPush(page1PushCallback);  // Page press event
page2.attachPush(page2PushCallback);  // Page press event
page3.attachPush(page3PushCallback);  // Page press event
page4.attachPush(page4PushCallback);  // Page press event

    
  // leds.setColorRGB(1, 0, 0, 0); //Calories LED Knowledge
}


void loop()
  
{
  
   s = s + 1;
  delay(1000);

  ////////////////////////////////////////////////////////////////////////////////////////////
       
if (s == 600) {
 btoff();
 off();
 
if (CurrentPage == 3){
 
   
    number14 == 1; // bath on
    bt3t.setValue(1); // bath on

PressureBar.setBits(0b000000000001111);
UsageBar.setBits(0b000000000001111);
EnviromentBar.setBits(0b000000000001111);

//face l
leds.setColorRGB(0, 255, 255, 0); // red for Europe
leds.setColorRGB(1, 255, 255, 0); // red for north america
leds.setColorRGB(2, 255, 255, 0); // red for Europe
leds.setColorRGB(3, 255, 255, 0); // red for north america
leds.setColorRGB(4, 255, 255, 0); // red for Europe
leds.setColorRGB(5, 255, 255, 0); // red for north america
leds.setColorRGB(6, 255, 255, 0); // red for Europe

yellow();
 
  }

  else if (CurrentPage == 2 || CurrentPage == 4 || CurrentPage == 1){
number2 == 1; // smart speaker
bt3l.setValue(1);

AudioBar.setBits(0b000000000001111);
UsageBar.setBits(0b000000000001111);
LocationBar.setBits(0b000000000001111); 

//face l 
leds.setColorRGB(0, 255, 0, 0); // red for autralia
leds.setColorRGB(1, 255, 0, 0); // red for asia
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(5, 255, 0, 0); // red for southamerica
leds.setColorRGB(6, 255, 0, 0); // red for unitedstates



red ();     

leds.setColorRGB(7, 0, 0, 0); // off for device manufacturere 
leds.setColorRGB(16,0, 0, 0); // off for research
  }
  } 


  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
else if (s == 1500){ // smart vacuum
btoff();
off();
LocationBar.setBits(0b000000000000000);

 if(CurrentPage == 4){
  
    number8 == 1; //fridge on 
    bt1k.setValue(1); // fridge is on
 
EnviromentBar.setBits(0b000000000000001); 
VisualBar.setBits(0b000000000000001);
LocationBar.setBits(0b000000000000001);
HealthlBar.setBits(0b000000000000001);  
UsageBar.setBits(0b000000000000001);

//face l 
leds.setColorRGB(1, 255, 0, 0); // red for north america
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(5, 255, 0, 0); // red for north america
leds.setColorRGB(6, 255, 0, 0); // red for Europe

red();
  }

else if (CurrentPage == 3){
number15 == 1; // soapdispenser on
bt1t.setValue(1); // soapdispenser on

PressureBar.setBits(0b000000000000011);
UsageBar.setBits(0b000000000000011);

//face l 
leds.setColorRGB(0, 255, 255, 0); // red for asia
leds.setColorRGB(1, 255, 255, 0); // red for autralia
leds.setColorRGB(3, 255, 255, 0); // red for Europe
leds.setColorRGB(5, 255, 255, 0); // red for southamerica
leds.setColorRGB(6, 255, 255, 0); // red for unitedstates




yellow ();

leds.setColorRGB(7, 0, 0, 0); // off for device manufacturere 
leds.setColorRGB(16,0, 0, 0); // off for research
  }


  else if (CurrentPage == 2 || CurrentPage == 1) {
    number1 == 1; // smart vacuum on
    bt1l.setValue(1); // smart vacuum on
   
   PressureBar.setBits(0b000000011111111);
   EnviromentBar.setBits(0b000000011111111);
   UsageBar.setBits(0b000000011111111);

leds.setColorRGB(1, 255, 255, 0); // yellow for asia
leds.setColorRGB(3, 255, 255, 0); // red for Europe
leds.setColorRGB(6, 255, 255, 0); // yellow for unitedstates

yellow();
  }
  }
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
else if (s == 2400) {          // camera on
btoff();
off();
 
if (CurrentPage == 3 || CurrentPage == 4){
  
    number11== 1; // faucet on
    bt6k.setValue(1); // faucet on 


AudioBar.setBits(0b000000011111111);
LocationBar.setBits(0b000000011111111); 
UsageBar.setBits(0b000000011111111);
PressureBar.setBits(0b000000011111111);


//face l 
leds.setColorRGB(0, 255, 0, 0); // red for asia
leds.setColorRGB(1, 255, 0, 0); // red for autralia
leds.setColorRGB(2, 255, 0, 0); // red for autralia
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(4, 255, 0, 0); // red for autralia
leds.setColorRGB(5, 255, 0, 0); // red for southamerica
leds.setColorRGB(6, 255, 0, 0); // red for unitedstates

red();
}


else if (CurrentPage == 2 || CurrentPage == 1){
 
number3 == 1; //camera on
bt6l.setValue(1); // camera on
    
camera ();
    
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
else if (s == 3300) {
btoff();
off();
   
}

/////////////////////////////////////////////////////////////////////
else if (s == 4200){ // lock
    btoff();
    off();

if (CurrentPage == 3) // bathroom mirror
{
 number13 == 1; // mirror on
 bt0t.setValue(1); // mirror on

PressureBar.setBits(0b000000000000001);
BiometricBar.setBits(0b000000000000001);
UsageBar.setBits(0b000000000000001);
EnviromentBar.setBits(0b000000000000001);

//face l

leds.setColorRGB(3, 255, 255, 0); // red for Europe
leds.setColorRGB(6, 255, 255, 0); // red for north america

yellow();
}

else if (CurrentPage == 1){
    
    number0 == 1; //smart lock on
    bt0l.setValue(1); // lock is on

EnviromentBar.setBits(0b000000000000011); 
BiometricBar.setBits(0b000000000000011);
AudioBar.setBits(0b000000000000011);
LocationBar.setBits(0b000000000000011);
VisualBar.setBits(0b000000000000011);  
UsageBar.setBits(0b000000000000011);

//face l 
//turn off all before starting
leds.setColorRGB(6, 255, 0, 0); // red for north america
leds.setColorRGB(3, 255, 0, 0); // red for Europe

red ();
leds.setColorRGB(15,255, 0, 0); // red for survellance
leds.setColorRGB(22,255, 0, 0); // red for security



  
}

else if (CurrentPage == 2){
 number12 == 1; // monitor on
 bt0b.setValue(1); // sleepmonitor on

PressureBar.setBits(0b000000000000011);
AudioBar.setBits(0b000000000000011);
BiometricBar.setBits(0b000000000000011);
HealthlBar.setBits(0b000000000000011);  
UsageBar.setBits(0b000000000000011);

//face l
leds.setColorRGB(0, 255, 0, 0); // red for north america
leds.setColorRGB(1, 255, 0, 0); // red for Europe
leds.setColorRGB(2, 255, 0, 0); // red for north america
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(4, 255, 0, 0); // red for north america
leds.setColorRGB(5, 255, 0, 0); // red for Europe
leds.setColorRGB(6, 255, 0, 0); // red for north america

red ();

}

else if (CurrentPage == 4){
    number9== 1; // coffee on
    bt0k.setValue(1); // coffee on 

UsageBar.setBits(0b000000000000011);

//face l 

leds.setColorRGB(3, 0, 204, 0); // green for Europe
leds.setColorRGB(1, 0, 204, 0);// greem for  asia

// rgb for smart lock, face B
leds.setColorRGB(8, 0, 204, 0); // yellow service provider 
leds.setColorRGB(9, 0, 204, 0); // yellow trusted party 
leds.setColorRGB(10, 0, 204, 0); // green for Resource owner
leds.setColorRGB(7, 0, 204, 0); // red for device manufacturer
leds.setColorRGB(14, 255, 255, 0); // red for law enforcemnt
leds.setColorRGB(12, 0, 204, 0); // red for third party
leds.setColorRGB(11, 0, 204, 0); // red for mareting orgnization

// rgb for smart fridge, face R
leds.setColorRGB(18,0, 204, 0); // yellow for revenue
leds.setColorRGB(17,0, 204, 0); // red for Anaytics
leds.setColorRGB(16,0, 204, 0); // red for research
leds.setColorRGB(21,0, 204, 0); // red for Targeted Ads
leds.setColorRGB(20,0, 204, 0); // red for Lifestyle
leds.setColorRGB(19,0, 204, 0); // red for Productivity

   //data retention   
for(int i=0; i<15; i++) { // For each pixel...
    pixels.setPixelColor(i, pixels.Color(0, 204, 0));
    pixels.show();   // Send the updated pixel colors to the hardware.

  }

}
}


/////////////////////////////////////////////////////////////////////////////////////////



else if(s==5100){ // thermo
 btoff();
 off();
  if (CurrentPage != 0){

    number6 == 1; //thermo on
    bt5l.setValue(1); // thermo is on

   
thermo ();
  }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  else if (s == 6000){
 btoff();
 off();
  if (CurrentPage != 0){
 
    number5 == 1; // light on
    bt4l.setValue(1); // light is on
 
light ();
  }
  }
///////////////////////////////////////////////////////////////////////////////////////////////////////////
else if (s == 6900) {
btoff();
off();
   
}

///////////////////////////////////////////////////////////////////////////////
else if ( s ==7800 ) //tv
{
    //turn off all before starting
    btoff();
    off();

    if (CurrentPage == 3) // bathroom toothbrush
{
    number16 == 1; // toothbrush on
    bt7t.setValue(1); // toothbrush on

PressureBar.setBits(0b000000000000111);
UsageBar.setBits(0b000000000000111);
BiometricBar.setBits(0b000000000000111);
HealthlBar.setBits(0b000000000000111);
    
//face l 
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(6, 255, 0, 0); // red for unitedstates

red ();
  }

  else if (CurrentPage == 2 || CurrentPage == 4 || CurrentPage == 1){
    number4 == 1; // tv on
    bt7l.setValue(1); // tv is on
    
BiometricBar.setBits(0b000000000000111);
AudioBar.setBits(0b000000000000111);
LocationBar.setBits(0b000000000000111);
VisualBar.setBits(0b000000000000111);  
UsageBar.setBits(0b000000000000111);

//face l 
leds.setColorRGB(1, 255, 0, 0); // red for north america
leds.setColorRGB(3, 255, 0, 0); // red for Europe
leds.setColorRGB(5, 255, 0, 0); // red for north america
leds.setColorRGB(6, 255, 0, 0); // red for Europe

red ();
}
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////
else if (s == 8700) // purifier
{
btoff();
off();
 
if (CurrentPage == 3 || CurrentPage == 4){

number10== 1; // fan on
bt2k.setValue(1); // fan on 

   LocationBar.setBits(0b000000001111111);
   EnviromentBar.setBits(0b000000001111111);
   UsageBar.setBits(0b000000001111111);
    
leds.setColorRGB(0, 255, 255, 0); // yellow for asia
leds.setColorRGB(1, 255, 255, 0); // red for Europe
leds.setColorRGB(2, 255, 255, 0); // yellow for unitedstates
leds.setColorRGB(3, 255, 255, 0); // yellow for asia
leds.setColorRGB(4, 255, 255, 0); // red for Europe
leds.setColorRGB(5, 255, 255, 0); // yellow for unitedstates
leds.setColorRGB(6, 255, 255, 0); // yellow for asia


yellow();
}

else if (CurrentPage == 2 || CurrentPage == 1){
    number7 == 1; // purifier on
    bt2l.setValue(1); // purifier is on
         
PressureBar.setBits(0b000000001111111);
EnviromentBar.setBits(0b000000001111111); 
LocationBar.setBits(0b000000001111111); 
UsageBar.setBits(0b000000001111111);

//face l 
leds.setColorRGB(0, 255, 255, 0); // red for north america
leds.setColorRGB(1, 255, 255, 0); // red for Europe
leds.setColorRGB(2, 255, 255, 0); // red for north america
leds.setColorRGB(3, 255, 255, 0); // red for Europe
leds.setColorRGB(4, 255, 255, 0); // red for north america
leds.setColorRGB(5, 255, 255, 0); // red for Europe
leds.setColorRGB(6, 255, 255, 0); // red for north america

yellow();
  
}
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

else if (s == 9600) {
btoff();
off();
   
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

 

   
  nexLoop(nex_listen_list);
  delay(10); // Delay a little bit to improve simulation performance
}
