#include "Nextion.h"
// #include "PrCFunctions.h"
#include <ChainableLED.h>
#include <Grove_LED_Bar.h>
#include "Adafruit_NeoPixel.h"
#include <SPI.h>
#include <WiFiEsp.h>
#include <WiFiEspServer.h>
#include <WiFiEspClient.h>
#include <ArduinoJson.h>
#define NUM_LEDS 23
char ssid[] = "shtb"; char pass[] = "a*******Z"; WiFiEspServer server(8888);
// numpofLEDpixels, Pin, NEO_GRB + NEO_KHZ800 ( sets the bits stream to RBG at 800khz) //clock pin, data pin, 23 LEDs, port D2
Adafruit_NeoPixel pixels(15, 3, NEO_GRB + NEO_KHZ800); ChainableLED leds(2, 3, NUM_LEDS);
//LED bars D4,D5,D6,D7,D8,A0,A1,A2,A3
 // Grove_LED_Bar: clock pin, data pin, orientation
Grove_LED_Bar LocationBar(5, 4, 0); Grove_LED_Bar PressureBar(6, 5, 0); Grove_LED_Bar VisualBar(7, 6, 0);
Grove_LED_Bar AudioBar(8, 7, 0); Grove_LED_Bar BiometricBar(9, 8, 0); Grove_LED_Bar HealthlBar(A1, A0, 0);
Grove_LED_Bar UsageBar(A2, A1, 0); Grove_LED_Bar EnviromentBar(A3, A2, 0);
// LED Colors
int R=100;int G=0;int B=0; int CurrentDevice=0;int CurrentPage = 0; uint32_t numbers[32]; uint32_t ButtonPress;
//Living Room Devices
// 0: Camera            4: Light
// 1: Lock              5: Thermostat
// 2: TV                6: Air Purifier
// 3: Voice Assistant   7: Vacuum
NexDSButton L1 = NexDSButton(1, 8, "bt6");    NexDSButton L5 = NexDSButton(1, 6, "bt4");NexDSButton L2 = NexDSButton(1, 3, "bt0");    NexDSButton L6 = NexDSButton(1, 7, "bt5");
NexDSButton L3 = NexDSButton(1, 9, "bt7");    NexDSButton L7 = NexDSButton(1, 10, "bt2");NexDSButton L4 = NexDSButton(1, 5, "bt3");    NexDSButton L8 = NexDSButton(1, 4, "bt1");
//Callback functions for LivingRoom Devices
void L1CBP(void *ptr){
  L1.getValue(&ButtonPress);
  int ID=1;Serial.print("CBFPush: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);
};
void L1CB(void *ptr){int ID=1;L1.getValue(&ButtonPress);L1.getValue(&ButtonPress);Serial.println(L1.setValue(1));numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void L2CB(void *ptr){int ID=2;L2.getValue(&ButtonPress);L2.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void L3CB(void *ptr){int ID=3;L3.getValue(&ButtonPress);L3.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void L4CB(void *ptr){int ID=4;L4.getValue(&ButtonPress);L4.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void L5CB(void *ptr){int ID=5;L5.getValue(&ButtonPress);L5.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void L6CB(void *ptr){int ID=6;L6.getValue(&ButtonPress);L7.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void L7CB(void *ptr){int ID=7;L7.getValue(&ButtonPress);L6.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void L8CB(void *ptr){int ID=8;L8.getValue(&ButtonPress);L8.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
//Bedroom Devices
// 0: Camera            4: Light
// 1: Sleep Monitor     5: Thermostat
// 2: TV                6: Air Purifier
// 3: Voice Assistant   7: Vacuum
NexDSButton BD1 = NexDSButton(2, 9, "bt6");  NexDSButton BD5 = NexDSButton(2, 7, "bt4");NexDSButton BD2 = NexDSButton(2, 6, "bt0");  NexDSButton BD6 = NexDSButton(2, 8, "bt5");
NexDSButton BD3 = NexDSButton(2, 10, "bt7"); NexDSButton BD7 = NexDSButton(2, 4, "bt2");NexDSButton BD4 = NexDSButton(2, 5, "bt3");  NexDSButton BD8 = NexDSButton(2, 3, "bt1");
//Callback functions for Bedroom Devices
void BD1CB(void *ptr){int ID=9;BD1.getValue(&ButtonPress);BD1.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void BD2CB(void *ptr){int ID=10;BD2.getValue(&ButtonPress);BD2.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void BD3CB(void *ptr){int ID=11;BD3.getValue(&ButtonPress);BD3.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void BD4CB(void *ptr){int ID=12;BD4.getValue(&ButtonPress);BD4.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void BD5CB(void *ptr){int ID=13;BD5.getValue(&ButtonPress);BD5.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void BD6CB(void *ptr){int ID=14;BD6.getValue(&ButtonPress);BD6.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void BD7CB(void *ptr){int ID=15;BD7.getValue(&ButtonPress);BD7.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void BD8CB(void *ptr){int ID=16;BD8.getValue(&ButtonPress);BD8.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
//Bathroom Devices
// 0: Mirror           4: Light
// 1: Soap Dispenser   5: Thermostat
// 2: Toothbrush       6: Extractor Fan
// 3: Bath             7: Faucet
NexDSButton T1 = NexDSButton(3, 3, "bt0");  NexDSButton T5 = NexDSButton(3, 9, "bt4");
NexDSButton T2 = NexDSButton(3, 7, "bt1");  NexDSButton T6 = NexDSButton(3, 10, "bt5");
NexDSButton T3 = NexDSButton(3, 8, "bt7");  NexDSButton T7 = NexDSButton(3, 5, "bt2");
NexDSButton T4 = NexDSButton(3, 4, "bt3");  NexDSButton T8 = NexDSButton(3, 6, "bt6");
void T1CB(void *ptr){int ID=17;T1.getValue(&ButtonPress);T1.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void T2CB(void *ptr){int ID=18;T2.getValue(&ButtonPress);T2.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void T3CB(void *ptr){int ID=19;T3.getValue(&ButtonPress);T3.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void T4CB(void *ptr){int ID=20;T4.getValue(&ButtonPress);T4.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void T5CB(void *ptr){int ID=21;T5.getValue(&ButtonPress);T5.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void T6CB(void *ptr){int ID=22;T6.getValue(&ButtonPress);T6.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void T7CB(void *ptr){int ID=23;T7.getValue(&ButtonPress);T7.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void T8CB(void *ptr){int ID=24;T8.getValue(&ButtonPress);T8.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
//Kitchen Devices
// 0: Fridge            4: Light
// 1: Coffee Machine    5: Thermostat
// 2: TV                6: Extractor Fan
// 3: Voice Assistant   7: Faucet
NexDSButton K1 = NexDSButton(4, 3, "bt1");  NexDSButton K5 = NexDSButton(4, 6, "bt4");NexDSButton K2 = NexDSButton(4, 8, "bt0");  NexDSButton K6 = NexDSButton(4, 7, "bt5");
NexDSButton K3 = NexDSButton(4, 9, "bt7");  NexDSButton K7 = NexDSButton(4, 4, "bt2");NexDSButton K4 = NexDSButton(4, 5, "bt3");  NexDSButton K8 = NexDSButton(4, 2, "bt6");
void K1CB(void *ptr){int ID=25;K1.getValue(&ButtonPress);K1.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void K2CB(void *ptr){int ID=26;K2.getValue(&ButtonPress);K2.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void K3CB(void *ptr){int ID=27;K3.getValue(&ButtonPress);K3.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void K4CB(void *ptr){int ID=28;K4.getValue(&ButtonPress);K4.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void K5CB(void *ptr){int ID=29;K5.getValue(&ButtonPress);K5.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void K6CB(void *ptr){int ID=30;K6.getValue(&ButtonPress);K6.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void K7CB(void *ptr){int ID=31;K7.getValue(&ButtonPress);K7.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
void K8CB(void *ptr){int ID=32;K8.getValue(&ButtonPress);K8.getValue(&ButtonPress);numbers[ID]=ButtonPress;Serial.print("CBF: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress==1 || ButtonPress==0){SetupLEDs(ID, false);}else{off();}}
NexPage page0 = NexPage(0, 0, "page0");  // Home Screen
NexPage page1 = NexPage(1, 0, "page1");  // Livingroom
NexPage page2 = NexPage(2, 0, "page2");  // Bedroom
NexPage page3 = NexPage(3, 0, "page3");  // Bathroom
NexPage page4 = NexPage(4, 0, "page4");  // Kitchen
void page0PushCallback(void *ptr){CurrentPage = 0;Serial.print("Page: ");Serial.println("Home");}void page1PushCallback(void *ptr){CurrentPage = 1;Serial.print("Page: ");Serial.println("Living Room");}
void page2PushCallback(void *ptr){CurrentPage = 2;Serial.print("Page: ");Serial.println("Bedroom");}void page3PushCallback(void *ptr){CurrentPage = 3;Serial.print("Page: ");Serial.println("Bathroom");}
void page4PushCallback(void *ptr){CurrentPage = 4;Serial.print("Page: ");Serial.println("Kitchen");}
NexTouch *nex_listen_list[] = {
  &L1,  &L2,  &L3,  &L4,  &L4,  &L6,  &L7,  &L8, &BD1,  &BD2,  &BD3,  &BD4,  &BD5,  &BD6,  &BD7,  &BD8,
  &T1,  &T2,  &T3,  &T4,  &T5,  &T6,  &T7,  &T8, &K1,  &K2,  &K3,  &K4,  &K5,  &K6,  &K7,  &K8,
  &page0,  &page1,  &page2,  &page3,  &page4, NULL };
struct Device {
  int deviceID;  String deviceType;  boolean location[7];  int dataRetentionTime;  bool locationEnabled;  bool presenceEnabled;  bool visualEnabled;  bool audioEnabled;  bool biometricsEnabled;
  bool healthEnabled;  bool usageEnabled;  bool environmentEnabled;  bool resourceOwnerAccess;  bool trustedPartyAccess;  bool serviceProviderAccess;  bool deviceManufacturerAccess;
  bool lawEnforcementAccess;  bool publicAccess;  bool thirdPartyAccess;  bool marketingOrganisationsAccess;  bool revenueUsage;  bool analyticsUsage;  bool researchUsage;  bool surveillanceUsage;
  bool securityUsage;  bool targetedAdsUsage;  bool lifeStyleUsage;  bool productivityUsage;};
Device devices[32];String LastStatus[32];
void SetupLEDs(int DSBtn, bool fromnetwork){
  Serial.print("Device: "); Serial.println(DSBtn);
  String deviceStatus=deviceToString(DSBtn);
  UpdateColor(devices[DSBtn].deviceType);
  if (LastStatus[DSBtn]==deviceStatus && CurrentDevice==DSBtn && fromnetwork){Serial.println("No update in the device");return;}
  else{
    off();
    CurrentDevice=DSBtn;
    LastStatus[DSBtn]=deviceToString(DSBtn);
  // Data Location and Retention
    for (int i=0; i<=6; i++){if (devices[DSBtn].location[i]){leds.setColorRGB(i, R, G, B);}}
    for (int i=0; i<=6; i++){if (devices[DSBtn].location[i]){leds.setColorRGB(i, R, G, B);}}
    for(int i=0; i<devices[DSBtn].dataRetentionTime; i++) {pixels.setPixelColor(i, pixels.Color(R, G, B));pixels.show();}
    // Data Type
    if (devices[DSBtn].locationEnabled){LocationBar.setBits(0x6);}if (devices[DSBtn].presenceEnabled){PressureBar.setBits(0x6);}
    if (devices[DSBtn].visualEnabled){VisualBar.setBits(0x6);}if (devices[DSBtn].audioEnabled){AudioBar.setBits(0x6);}
    if (devices[DSBtn].biometricsEnabled){BiometricBar.setBits(0x6);}if (devices[DSBtn].healthEnabled){HealthlBar.setBits(0x6);}
    if (devices[DSBtn].usageEnabled){UsageBar.setBits(0x6);}if (devices[DSBtn].environmentEnabled){EnviromentBar.setBits(0x6);}
    //Data Usage
    if (devices[DSBtn].surveillanceUsage){leds.setColorRGB(7, R, G, B);}if (devices[DSBtn].researchUsage){leds.setColorRGB(8, R, G, B);}
    if (devices[DSBtn].analyticsUsage){leds.setColorRGB(9, R, G, B);}if (devices[DSBtn].revenueUsage){leds.setColorRGB(10, R, G, B);}
    if (devices[DSBtn].productivityUsage){leds.setColorRGB(11, R, G, B);}if (devices[DSBtn].lifeStyleUsage){leds.setColorRGB(12, R, G, B);}
    if (devices[DSBtn].targetedAdsUsage){leds.setColorRGB(13, R, G, B);}if (devices[DSBtn].securityUsage){leds.setColorRGB(14, R, G, B);}
    // Data Access
    if (devices[DSBtn].deviceManufacturerAccess){leds.setColorRGB(15, R, G, B);}if (devices[DSBtn].serviceProviderAccess){leds.setColorRGB(16, R, G, B);}
    if (devices[DSBtn].thirdPartyAccess){leds.setColorRGB(17, R, G, B);}if (devices[DSBtn].resourceOwnerAccess){leds.setColorRGB(18, R, G, B);}
    if (devices[DSBtn].marketingOrganisationsAccess){leds.setColorRGB(19, R, G, B);}if (devices[DSBtn].trustedPartyAccess){leds.setColorRGB(20, R, G, B);}
    if (devices[DSBtn].publicAccess){leds.setColorRGB(21, R, G, B);}if (devices[DSBtn].lawEnforcementAccess){leds.setColorRGB(22, R, G, B);}
  }
}
void off(){
  LocationBar.setBits(0x0);  PressureBar.setBits(0x0);  VisualBar.setBits(0x0);  AudioBar.setBits(0x0);
  HealthlBar.setBits(0x0);  UsageBar.setBits(0x0);  EnviromentBar.setBits(0x0);  BiometricBar.setBits(0x0);
  for (int i=0; i<=22; i++){ leds.setColorRGB(i, 0, 0, 0);}for(int i=0; i<15; i++) { pixels.setPixelColor(i, pixels.Color(0, 0, 0));  pixels.show(); }
}
void btoff(){
    L1.setValue(0);L2.setValue(0);L3.setValue(0);L4.setValue(0);L5.setValue(0);L6.setValue(0);L7.setValue(0);L8.setValue(0);
    K1.setValue(0);K2.setValue(0);K3.setValue(0);K4.setValue(0);K5.setValue(0);K6.setValue(0);K7.setValue(0);K8.setValue(0);
    T1.setValue(0);T2.setValue(0);T3.setValue(0);T4.setValue(0);T5.setValue(0);T6.setValue(0);T7.setValue(0);T8.setValue(0);
    BD1.setValue(0);BD2.setValue(0);BD3.setValue(0);BD4.setValue(0);BD5.setValue(0);BD6.setValue(0);BD7.setValue(0);BD8.setValue(0);
}
void startUpTest(){
  LocationBar.setBits(0x1);  delay(500);  LocationBar.setBits(0x0);  PressureBar.setBits(0x1);  delay(500);  PressureBar.setBits(0x0); 
  VisualBar.setBits(0x1);    delay(500);  VisualBar.setBits(0x0);    AudioBar.setBits(0x1);     delay(500);  AudioBar.setBits(0x0);
  BiometricBar.setBits(0x1); delay(500);  BiometricBar.setBits(0x0); HealthlBar.setBits(0x1);   delay(500);  HealthlBar.setBits(0x0);
  UsageBar.setBits(0x1);     delay(500);  UsageBar.setBits(0x0);     EnviromentBar.setBits(0x1);delay(500);  EnviromentBar.setBits(0x0);
  for (int i=0; i<=6; i++){leds.setColorRGB(i, R, G, B);    delay(1000);    leds.setColorRGB(i, 0, 0, 0);}
  for (int i=7; i<=22; i++){leds.setColorRGB(i, R, G, B); delay(500); leds.setColorRGB(i, 0, 0, 0);}
  for(int i=0; i<15; i++) {pixels.setPixelColor(i, pixels.Color(R, G, B)); pixels.show(); delay(500);}
  for(int i=15; i>0; i--) {pixels.setPixelColor(i, pixels.Color(0, 0, 0));pixels.show();delay(500);}
  off();
}
bool CheckSign(const String& str, const String& prefix) {
    if (str.length() < prefix.length()) {return false;}
    for (size_t i = 0; i < prefix.length(); ++i) {
        if (str.charAt(i) != prefix.charAt(i)) {return false;}
    }return true;
}
void setup() {
  off();btoff();
  Serial3.begin(115200);Serial.begin(115200);Serial2.begin(9600);
  WiFi.init(&Serial3);
  if (WiFi.status() == WL_NO_SHIELD) {Serial.println("WiFi module not detected");while (true);}
  Serial.print("Connecting to ");Serial.println(ssid);
  while (WiFi.status() != WL_CONNECTED){WiFi.begin(ssid, pass);delay(1000);}
  nexInit(); pixels.begin();
  L1.attachPop(L1CB);L2.attachPop(L2CB);L3.attachPop(L3CB);L4.attachPop(L4CB);L5.attachPop(L5CB);L6.attachPop(L6CB);L7.attachPop(L7CB);L8.attachPop(L8CB);
  BD1.attachPop(BD1CB);BD2.attachPop(BD2CB);BD3.attachPop(BD3CB);BD4.attachPop(BD4CB);BD5.attachPop(BD5CB);BD6.attachPop(BD6CB);BD7.attachPop(BD7CB);BD8.attachPop(BD8CB);
  T1.attachPop(T1CB);T2.attachPop(T2CB);T3.attachPop(T3CB);T4.attachPop(T4CB);T5.attachPop(T5CB);T6.attachPop(T6CB);T7.attachPop(T7CB);T8.attachPop(T8CB);
  K1.attachPop(K1CB);K2.attachPop(K2CB);K3.attachPop(K3CB);K4.attachPop(K4CB);K5.attachPop(K5CB);K6.attachPop(K6CB);K7.attachPop(K7CB);K8.attachPop(K8CB);
  
  L1.attachPush(L1CBP);
  
  page0.attachPush(page0PushCallback);page1.attachPush(page1PushCallback);page2.attachPush(page2PushCallback);page3.attachPush(page3PushCallback);
  page4.attachPush(page4PushCallback);Serial.print("Device IP from DHCP: ");Serial.println(WiFi.localIP());
  server.begin();Serial.println("Server started");
}
void loop() {
  WiFiEspClient client = server.available();
  if (!client) { nexLoop(nex_listen_list); delay(50); return;}
  while (client.connected() && !client.available()){delay(1);}
  String jsonBuffer = "";
  int limit = 0;
  bool startsign=false;
  bool endsign=false;
  bool linebreak=false;
  bool limitexceed=false;
  while(client.connected()){
    if (client.available()) {char c = client.read();
      if (c == '\n'){linebreak=true;break;}
      if (limit>=41){limitexceed=true;break;}
      jsonBuffer += c;limit++;
        // client.println("NA");client.stop();Serial.println("Limit exceed for Incoming Packet:");
        
        
        // Serial.println(jsonBuffer);nexLoop(nex_listen_list);delay(50);return;
      // else if (endsign && !startsign){
      //   {client.println("NA");client.stop();Serial.println("Error in Incoming Packet:");Serial.println(jsonBuffer);nexLoop(nex_listen_list);delay(50);return;}
    }
  }
  // Serial.println(limit);
  // Serial.println(jsonBuffer);
  if (isValidString(jsonBuffer)){
    client.println("OK");
    int dev = decodeStringToDevice(jsonBuffer);
    // processInput(jsonBuffer);
    SetupLEDs(dev, true);
  }
  else{
    client.println("NA");Serial.println("Error in Incoming Packet:");
  }
  // else{
  //   Serial.println("The above string has problems.");
  //   Serial.print("Started: ");
  //   Serial.print(startsign);
  //   Serial.print(", Ended: ");
  //   Serial.print(endsign);
  //   Serial.print(", Line break: " );
  //   Serial.print(linebreak);
  //   Serial.print(", Limit Exceeded: " );
  //   Serial.println(limitexceed);
  // }
  while (client.available()) {
    client.read();
  }
  client.stop();
  nexLoop(nex_listen_list); delay(50);
}
void processInput(String devDetails){
  int index = (devDetails.substring(0, 2)).toInt();
  Serial.println("Before updating.");
  deviceStatusString(index);
  devices[index].deviceType = devDetails.substring(2, 4);
  for (int i = 0; i < 7; i++) {
    devices[index].location[i] = (devDetails.charAt(4 + i) == '1');
  }
  devices[index].dataRetentionTime = (devDetails.substring(11, 13)).toInt();
  devices[index].locationEnabled = (devDetails.charAt(13) == '1');
  devices[index].presenceEnabled = (devDetails.charAt(14) == '1');
  devices[index].visualEnabled = (devDetails.charAt(15) == '1');
  devices[index].audioEnabled = (devDetails.charAt(16) == '1');
  devices[index].biometricsEnabled = (devDetails.charAt(17) == '1');
  devices[index].healthEnabled = (devDetails.charAt(18) == '1');
  devices[index].usageEnabled = (devDetails.charAt(19) == '1');
  devices[index].environmentEnabled = (devDetails.charAt(20) == '1');
  devices[index].resourceOwnerAccess = (devDetails.charAt(21) == '1');
  devices[index].trustedPartyAccess = (devDetails.charAt(22) == '1');
  devices[index].serviceProviderAccess = (devDetails.charAt(23) == '1');
  devices[index].deviceManufacturerAccess = (devDetails.charAt(24) == '1');
  devices[index].lawEnforcementAccess = (devDetails.charAt(25) == '1');
  devices[index].publicAccess = (devDetails.charAt(26) == '1');
  devices[index].thirdPartyAccess = (devDetails.charAt(27) == '1');
  devices[index].marketingOrganisationsAccess = (devDetails.charAt(28) == '1');
  devices[index].revenueUsage = (devDetails.charAt(29) == '1');
  devices[index].analyticsUsage = (devDetails.charAt(30) == '1');
  devices[index].researchUsage = (devDetails.charAt(31) == '1');
  devices[index].surveillanceUsage = (devDetails.charAt(32) == '1');
  devices[index].securityUsage = (devDetails.charAt(33) == '1');
  devices[index].targetedAdsUsage = (devDetails.charAt(34) == '1');
  devices[index].lifeStyleUsage = (devDetails.charAt(35) == '1');
  devices[index].productivityUsage = (devDetails.charAt(36) == '1');
  Serial.println("After updating.");
  deviceStatusString(index);
}
// void processInput(JsonObject doc) {
//       int index = doc["Start"]["DeviceDetails"]["DeviceID"].as<int>();
//       for (int i = 0; i<=6; i++){String j = String(i);devices[index].location[i] = doc["Start"]["DataStorage"]["Location"][j].as<bool>();}
//       devices[index].deviceType = doc["Start"]["DeviceDetails"]["DeviceType"].as<String>();
//       devices[index].dataRetentionTime = doc["Start"]["DataStorage"]["DataRetentionTime"].as<int>();
//       devices[index].locationEnabled = doc["Start"]["DataType"]["Location"].as<bool>();
//       devices[index].presenceEnabled = doc["Start"]["DataType"]["Presence"].as<bool>();
//       devices[index].visualEnabled = doc["Start"]["DataType"]["Visual"].as<bool>();
//       devices[index].audioEnabled = doc["Start"]["DataType"]["Audio"].as<bool>();
//       devices[index].biometricsEnabled = doc["Start"]["DataType"]["Biometrics"].as<bool>();
//       devices[index].healthEnabled = doc["Start"]["DataType"]["Health"].as<bool>();
//       devices[index].usageEnabled = doc["Start"]["DataType"]["Usage"].as<bool>();
//       devices[index].environmentEnabled = doc["Start"]["DataType"]["Environment"].as<bool>();
//       devices[index].resourceOwnerAccess = doc["Start"]["DataAccess"]["ResourceOwner"].as<bool>();
//       devices[index].trustedPartyAccess = doc["Start"]["DataAccess"]["TrustedParty"].as<bool>();
//       devices[index].serviceProviderAccess = doc["Start"]["DataAccess"]["ServiceProvider"].as<bool>();
//       devices[index].deviceManufacturerAccess = doc["Start"]["DataAccess"]["DeviceManufacturer"].as<bool>();
//       devices[index].lawEnforcementAccess = doc["Start"]["DataAccess"]["LawEnforcement"].as<bool>();
//       devices[index].publicAccess = doc["Start"]["DataAccess"]["Public"].as<bool>();
//       devices[index].thirdPartyAccess = doc["Start"]["DataAccess"]["ThirdParty"].as<bool>();
//       devices[index].marketingOrganisationsAccess = doc["Start"]["DataAccess"]["MarketingOrganisations"].as<bool>();
//       devices[index].revenueUsage = doc["Start"]["DataUsage"]["Revenue"].as<bool>();
//       devices[index].analyticsUsage = doc["Start"]["DataUsage"]["Analytics"].as<bool>();
//       devices[index].researchUsage = doc["Start"]["DataUsage"]["Research"].as<bool>();
//       devices[index].surveillanceUsage = doc["Start"]["DataUsage"]["Surveillance"].as<bool>();
//       devices[index].securityUsage = doc["Start"]["DataUsage"]["Security"].as<bool>();
//       devices[index].targetedAdsUsage = doc["Start"]["DataUsage"]["TargetedAds"].as<bool>();
//       devices[index].lifeStyleUsage = doc["Start"]["DataUsage"]["LifeStyle"].as<bool>();
//       devices[index].productivityUsage = doc["Start"]["DataUsage"]["Productivity"].as<bool>();
//       deviceStatus(index);
// }
void deviceStatusString(int dID){
  int index = dID;
  Serial.print(index);
  Serial.print(devices[index].deviceType);
  for (int i = 0; i < 7; i++) {
    Serial.print(devices[index].location[i]);
  }
  Serial.print(devices[index].dataRetentionTime);
  Serial.print(devices[index].locationEnabled);
  Serial.print(devices[index].presenceEnabled);
  Serial.print(devices[index].visualEnabled);
  Serial.print(devices[index].audioEnabled);
  Serial.print(devices[index].biometricsEnabled);
  Serial.print(devices[index].healthEnabled);
  Serial.print(devices[index].usageEnabled);
  Serial.print(devices[index].environmentEnabled);
  Serial.print(devices[index].resourceOwnerAccess);
  Serial.print(devices[index].trustedPartyAccess);
  Serial.print(devices[index].serviceProviderAccess);
  Serial.print(devices[index].deviceManufacturerAccess);
  Serial.print(devices[index].lawEnforcementAccess);
  Serial.print(devices[index].publicAccess);
  Serial.print(devices[index].thirdPartyAccess);
  Serial.print(devices[index].marketingOrganisationsAccess);
  Serial.print(devices[index].revenueUsage);
  Serial.print(devices[index].analyticsUsage);
  Serial.print(devices[index].researchUsage);
  Serial.print(devices[index].surveillanceUsage);
  Serial.print(devices[index].securityUsage);
  Serial.print(devices[index].targetedAdsUsage);
  Serial.print(devices[index].lifeStyleUsage);
  Serial.print(devices[index].productivityUsage);
  Serial.println();
}
String deviceToString(int dID) {
  String result = "";
  result += String(devices[dID].deviceID);
  result += devices[dID].deviceType;
  for (int i = 0; i < 7; i++) {
    result += devices[dID].location[i] ? '1' : '0';
  }
  result += String(devices[dID].dataRetentionTime);
  result += devices[dID].locationEnabled ? '1' : '0';
  result += devices[dID].presenceEnabled ? '1' : '0';
  result += devices[dID].visualEnabled ? '1' : '0';
  result += devices[dID].audioEnabled ? '1' : '0';
  result += devices[dID].biometricsEnabled ? '1' : '0';
  result += devices[dID].healthEnabled ? '1' : '0';
  result += devices[dID].usageEnabled ? '1' : '0';
  result += devices[dID].environmentEnabled ? '1' : '0';
  result += devices[dID].resourceOwnerAccess ? '1' : '0';
  result += devices[dID].trustedPartyAccess ? '1' : '0';
  result += devices[dID].serviceProviderAccess ? '1' : '0';
  result += devices[dID].deviceManufacturerAccess ? '1' : '0';
  result += devices[dID].lawEnforcementAccess ? '1' : '0';
  result += devices[dID].publicAccess ? '1' : '0';
  result += devices[dID].thirdPartyAccess ? '1' : '0';
  result += devices[dID].marketingOrganisationsAccess ? '1' : '0';
  result += devices[dID].revenueUsage ? '1' : '0';
  result += devices[dID].analyticsUsage ? '1' : '0';
  result += devices[dID].researchUsage ? '1' : '0';
  result += devices[dID].surveillanceUsage ? '1' : '0';
  result += devices[dID].securityUsage ? '1' : '0';
  result += devices[dID].targetedAdsUsage ? '1' : '0';
  result += devices[dID].lifeStyleUsage ? '1' : '0';
  result += devices[dID].productivityUsage ? '1' : '0';
  return result;
}
void deviceStatus(int dID) {
  if (dID < 0 || dID >= 32) {Serial.println("Invalid device ID");return;}
  Serial.print("Printing Report of Device ID:"); Serial.print(dID);  Serial.print(", Type:");
  Serial.println(devices[dID].deviceType);  Serial.print("Locations: ");
  for (int i=0; i<=6; i++){Serial.print(i); Serial.print(":"); Serial.print(devices[dID].location[i] ? 'Y' : 'N');Serial.print(", ");}
  Serial.println();Serial.print("Retention Time:");
  Serial.println(devices[dID].dataRetentionTime);  Serial.print("Location Data Enabled:");
  Serial.print(devices[dID].locationEnabled ? 'Y' : 'N');  Serial.print(", Presense Data Enabled: ");
  Serial.print(devices[dID].presenceEnabled ? 'Y' : 'N');  Serial.print(", Vision Data Enabled: ");
  Serial.print(devices[dID].visualEnabled ? 'Y' : 'N');  Serial.print(", Audio Data Enabled: ");
  Serial.print(devices[dID].audioEnabled ? 'Y' : 'N');  Serial.print(", Biometrics Data Enabled: ");
  Serial.print(devices[dID].biometricsEnabled ? 'Y' : 'N');  Serial.print(", Health  Data Enabled :");
  Serial.print(devices[dID].healthEnabled ? 'Y' : 'N');  Serial.print(", Usage Data Enabled: ");
  Serial.print(devices[dID].usageEnabled ? 'Y' : 'N');  Serial.print(", Env Data Enabled: ");
  Serial.println(devices[dID].environmentEnabled ? 'Y' : 'N');  Serial.print("Resouse Owner Access: ");
  Serial.print(devices[dID].resourceOwnerAccess ? 'Y' : 'N');  Serial.print(", Trust Third Party Access :");
  Serial.print(devices[dID].trustedPartyAccess ? 'Y' : 'N');  Serial.print(", Service Provider Access :");
  Serial.print(devices[dID].serviceProviderAccess ? 'Y' : 'N');  Serial.print(", Device Manufacturer Access :");
  Serial.print(devices[dID].deviceManufacturerAccess ? 'Y' : 'N');  Serial.print(", Law Enforcement Access :");
  Serial.print(devices[dID].lawEnforcementAccess ? 'Y' : 'N');  Serial.print(", Public Access: ");
  Serial.print(devices[dID].publicAccess ? 'Y' : 'N');  Serial.print(", Third Party Access: ");
  Serial.print(devices[dID].thirdPartyAccess ? 'Y' : 'N');  Serial.print(", Marketing Organisations Access: ");
  Serial.println(devices[dID].marketingOrganisationsAccess ? 'Y' : 'N');  Serial.print("Revenue Usage: ");
  Serial.print(devices[dID].revenueUsage ? 'Y' : 'N');  Serial.print(", Analytics Usage: ");
  Serial.print(devices[dID].analyticsUsage ? 'Y' : 'N');  Serial.print(", Research Usage: ");
  Serial.print(devices[dID].researchUsage ? 'Y' : 'N');  Serial.print(", Surveillance Usage: ");
  Serial.print(devices[dID].surveillanceUsage ? 'Y' : 'N');  Serial.print(", Security Usage: ");
  Serial.print(devices[dID].securityUsage ? 'Y' : 'N');  Serial.print(", TargetedAds Usage: ");
  Serial.print(devices[dID].targetedAdsUsage ? 'Y' : 'N');  Serial.print(", LifeStyle Usage: ");
  Serial.print(devices[dID].lifeStyleUsage ? 'Y' : 'N');  Serial.print(", Productivity Usage: ");
  Serial.println(devices[dID].productivityUsage ? 'Y' : 'N');
}
void UpdateColor(String DeviceType){
  if (DeviceType=="TV"){R=0;    G=0;    B=255;} // Television
  else if (DeviceType=="CM"){R=255; G=0;  B=0;} // Coffee Machine
  else if (DeviceType=="VD"){R=255; G=0;  B=0;} // Video Device (Camera)
  else if (DeviceType=="LK"){R=0; G=255;  B=0;} //Lock
  else if (DeviceType=="VA"){R=255; G=128;  B=0;} // Voice Assistant
  else if (DeviceType=="LT"){R=0; G=255;  B=255;} // Light
  else if (DeviceType=="TM"){R=255; G=255;  B=0;} // Thermostat
  else if (DeviceType=="AP"){R=255; G=255;  B=0;} // Air Purifier
  else if (DeviceType=="VC"){R=255; G=255;  B=0;} // Vacuum Cleaner
  else if (DeviceType=="SM"){R=255; G=255;  B=0;} // Smart Mirror
  else if (DeviceType=="FT"){R=255; G=255;  B=0;} // Faucet
  else if (DeviceType=="SD"){R=255; G=255;  B=0;} // Soap Dispenser
  else if (DeviceType=="TB"){R=255; G=255;  B=0;} // Toothbrush
  else if (DeviceType=="BH"){R=255; G=255;  B=0;} // Bath
  else if (DeviceType=="FN"){R=255; G=255;  B=0;} // Exhaust Fan
  else if (DeviceType=="FD"){R=255; G=255;  B=0;} // Fridge
  else{R=0;G=0;B=0;} // No Device Type (LEDs Off)
}
bool isValidString(String str) {
  return true;
    // char startChar = "#";
    // char endChar = "!";
    // int length = str.length();
    // Serial.println(str[0]);
    // Serial.println(str[length - 1]);
    // if ((int)str[0] != 35) {
    //     return false;
    // }
    // if ((int)str[length - 1] != 33) {
    //     return false;
    // }
    // return true;
}
int decodeStringToDevice(String encodedStr) {
  int deviceID = encodedStr.substring(0, 2).toInt();
  // Serial.println(encodedStr);
  // Serial.print("Device ID: ");
  // Serial.println(deviceID);
  // Serial.println("Before processing: ");
  // deviceStatusString(deviceID);
  devices[deviceID].deviceType = encodedStr.substring(2, 4);
  // Serial.println(devices[deviceID].deviceType);
  byte locs = encodedStr.substring(4, 7).toInt();
  // Serial.println(locs);
  for (int i = 6; i >= 0; i--) {
    devices[deviceID].location[i] = bitRead(locs, i);
    // Serial.print(devices[deviceID].location[i]); Serial.print(", ");
  }
  // Serial.println();
  devices[deviceID].dataRetentionTime = encodedStr.substring(7, 9).toInt();
  // Serial.println(devices[deviceID].dataRetentionTime);
  byte dataEnabledByte = encodedStr.substring(9, 12).toInt();
  // Serial.println(dataEnabledByte);
  devices[deviceID].locationEnabled = bitRead(dataEnabledByte, 7);
  devices[deviceID].presenceEnabled = bitRead(dataEnabledByte, 6);
  devices[deviceID].visualEnabled = bitRead(dataEnabledByte, 5);
  devices[deviceID].audioEnabled = bitRead(dataEnabledByte, 4);
  devices[deviceID].biometricsEnabled = bitRead(dataEnabledByte, 3);
  devices[deviceID].healthEnabled = bitRead(dataEnabledByte, 2);
  devices[deviceID].usageEnabled = bitRead(dataEnabledByte, 1);
  devices[deviceID].environmentEnabled = bitRead(dataEnabledByte, 0);
  byte dataAccessByte = encodedStr.substring(12, 15).toInt();
  // Serial.println(dataAccessByte);
  devices[deviceID].resourceOwnerAccess = bitRead(dataAccessByte, 7);
  devices[deviceID].trustedPartyAccess = bitRead(dataAccessByte, 6);
  devices[deviceID].serviceProviderAccess = bitRead(dataAccessByte, 5);
  devices[deviceID].deviceManufacturerAccess = bitRead(dataAccessByte, 4);
  devices[deviceID].lawEnforcementAccess = bitRead(dataAccessByte, 3);
  devices[deviceID].publicAccess = bitRead(dataAccessByte, 2);
  devices[deviceID].thirdPartyAccess = bitRead(dataAccessByte, 1);
  devices[deviceID].marketingOrganisationsAccess = bitRead(dataAccessByte, 0); 
  byte dataUsageByte = encodedStr.substring(15, 18).toInt();
  //  Serial.println(dataUsageByte);
  devices[deviceID].revenueUsage = bitRead(dataUsageByte, 7);
  devices[deviceID].analyticsUsage = bitRead(dataUsageByte, 6);
  devices[deviceID].researchUsage = bitRead(dataUsageByte, 5);
  devices[deviceID].surveillanceUsage = bitRead(dataUsageByte, 4);
  devices[deviceID].securityUsage = bitRead(dataUsageByte, 3);
  devices[deviceID].targetedAdsUsage = bitRead(dataUsageByte, 2);
  devices[deviceID].lifeStyleUsage = bitRead(dataUsageByte, 1);
  devices[deviceID].productivityUsage = bitRead(dataUsageByte, 0);
  // Serial.println("After processing: ");
  // deviceStatusString(deviceID);
  // deviceStatus(deviceID);
  return deviceID;
}