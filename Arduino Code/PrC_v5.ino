#include "Nextion.h"
#include <ChainableLED.h>
#include <Grove_LED_Bar.h>
#include "Adafruit_NeoPixel.h"
// #include <SPI.h>
#include <WiFiS3.h>
#include <ArduinoJson.h>
#define NUM_LEDS 23
char ssid[] = "shtb"; char pass[] = "a*******Z"; WiFiServer server(8888);int status = WL_IDLE_STATUS; 
Adafruit_NeoPixel pixels(15, 3, NEO_GRB + NEO_KHZ800); ChainableLED leds(2, 3, NUM_LEDS);
Grove_LED_Bar LocationBar(5, 4, 0); Grove_LED_Bar PressureBar(6, 5, 0); Grove_LED_Bar VisualBar(7, 6, 0);
Grove_LED_Bar AudioBar(8, 7, 0); Grove_LED_Bar BiometricBar(9, 8, 0); Grove_LED_Bar HealthlBar(A1, A0, 0);
Grove_LED_Bar UsageBar(A2, A1, 0); Grove_LED_Bar EnviromentBar(A3, A2, 0);
int R=100;int G=0;int B=0; int CurrentDevice=0;int CurrentPage = 0; uint32_t ButtonPress;
NexDSButton L1 = NexDSButton(1, 8, "bt6");    NexDSButton L5 = NexDSButton(1, 6, "bt4");NexDSButton L2 = NexDSButton(1, 3, "bt0");    NexDSButton L6 = NexDSButton(1, 7, "bt5");
NexDSButton L3 = NexDSButton(1, 9, "bt7");    NexDSButton L7 = NexDSButton(1, 10, "bt2");NexDSButton L4 = NexDSButton(1, 5, "bt3");    NexDSButton L8 = NexDSButton(1, 4, "bt1");
void L1CB(void *ptr){int ID=1;L1.getValue(&ButtonPress);L1.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();L1.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void L2CB(void *ptr){int ID=2;L2.getValue(&ButtonPress);L2.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();L2.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void L3CB(void *ptr){int ID=3;L3.getValue(&ButtonPress);L3.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();L3.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void L4CB(void *ptr){int ID=4;L4.getValue(&ButtonPress);L4.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();L4.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void L5CB(void *ptr){int ID=5;L5.getValue(&ButtonPress);L5.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();L5.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void L6CB(void *ptr){int ID=6;L6.getValue(&ButtonPress);L7.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();L6.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void L7CB(void *ptr){int ID=7;L7.getValue(&ButtonPress);L6.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();L7.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void L8CB(void *ptr){int ID=8;L8.getValue(&ButtonPress);L8.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();L8.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
NexDSButton BD1 = NexDSButton(2, 9, "bt6");  NexDSButton BD5 = NexDSButton(2, 7, "bt4");NexDSButton BD2 = NexDSButton(2, 6, "bt0");  NexDSButton BD6 = NexDSButton(2, 8, "bt5");
NexDSButton BD3 = NexDSButton(2, 10, "bt7"); NexDSButton BD7 = NexDSButton(2, 4, "bt2");NexDSButton BD4 = NexDSButton(2, 5, "bt3");  NexDSButton BD8 = NexDSButton(2, 3, "bt1");
void BD1CB(void *ptr){int ID=9;BD1.getValue(&ButtonPress);BD1.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();BD1.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void BD2CB(void *ptr){int ID=10;BD2.getValue(&ButtonPress);BD2.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();BD2.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void BD3CB(void *ptr){int ID=11;BD3.getValue(&ButtonPress);BD3.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();BD3.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void BD4CB(void *ptr){int ID=12;BD4.getValue(&ButtonPress);BD4.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();BD4.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void BD5CB(void *ptr){int ID=13;BD5.getValue(&ButtonPress);BD5.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();BD5.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void BD6CB(void *ptr){int ID=14;BD6.getValue(&ButtonPress);BD6.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();BD6.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void BD7CB(void *ptr){int ID=15;BD7.getValue(&ButtonPress);BD7.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();BD7.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void BD8CB(void *ptr){int ID=16;BD8.getValue(&ButtonPress);BD8.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();BD8.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
NexDSButton T1 = NexDSButton(3, 3, "bt0");  NexDSButton T5 = NexDSButton(3, 9, "bt4");
NexDSButton T2 = NexDSButton(3, 7, "bt1");  NexDSButton T6 = NexDSButton(3, 10, "bt5");
NexDSButton T3 = NexDSButton(3, 8, "bt7");  NexDSButton T7 = NexDSButton(3, 5, "bt2");
NexDSButton T4 = NexDSButton(3, 4, "bt3");  NexDSButton T8 = NexDSButton(3, 6, "bt6");
void T1CB(void *ptr){int ID=17;T1.getValue(&ButtonPress);T1.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();T1.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void T2CB(void *ptr){int ID=18;T2.getValue(&ButtonPress);T2.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();T2.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void T3CB(void *ptr){int ID=19;T3.getValue(&ButtonPress);T3.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();T3.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void T4CB(void *ptr){int ID=20;T4.getValue(&ButtonPress);T4.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();T4.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void T5CB(void *ptr){int ID=21;T5.getValue(&ButtonPress);T5.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();T5.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void T6CB(void *ptr){int ID=22;T6.getValue(&ButtonPress);T6.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();T6.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void T7CB(void *ptr){int ID=23;T7.getValue(&ButtonPress);T7.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();T7.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void T8CB(void *ptr){int ID=24;T8.getValue(&ButtonPress);T8.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();T8.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
NexDSButton K1 = NexDSButton(4, 3, "bt1");  NexDSButton K5 = NexDSButton(4, 6, "bt4");NexDSButton K2 = NexDSButton(4, 8, "bt0");  NexDSButton K6 = NexDSButton(4, 7, "bt5");
NexDSButton K3 = NexDSButton(4, 9, "bt7");  NexDSButton K7 = NexDSButton(4, 4, "bt2");NexDSButton K4 = NexDSButton(4, 5, "bt3");  NexDSButton K8 = NexDSButton(4, 2, "bt6");
void K1CB(void *ptr){int ID=25;K1.getValue(&ButtonPress);K1.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();K1.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void K2CB(void *ptr){int ID=26;K2.getValue(&ButtonPress);K2.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();K2.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void K3CB(void *ptr){int ID=27;K3.getValue(&ButtonPress);K3.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();K3.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void K4CB(void *ptr){int ID=28;K4.getValue(&ButtonPress);K4.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();K4.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void K5CB(void *ptr){int ID=29;K5.getValue(&ButtonPress);K5.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();K5.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void K6CB(void *ptr){int ID=30;K6.getValue(&ButtonPress);K6.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();K6.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void K7CB(void *ptr){int ID=31;K7.getValue(&ButtonPress);K7.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();K7.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
void K8CB(void *ptr){int ID=32;K8.getValue(&ButtonPress);K8.getValue(&ButtonPress);Serial.print("Device ID | Status: ");Serial.print(ID);Serial.print(":");Serial.println(ButtonPress);if (ButtonPress){btoff();K8.setValue(1);SetupLEDs(ID, false);}else{btoff();off();}}
NexPage page0 = NexPage(0, 0, "page0");  // Home Screen
NexPage page1 = NexPage(1, 0, "page1");  // Livingroom
NexPage page2 = NexPage(2, 0, "page2");  // Bedroom
NexPage page3 = NexPage(3, 0, "page3");  // Bathroom
NexPage page4 = NexPage(4, 0, "page4");  // Kitchen
void page0PushCallback(void *ptr){CurrentPage = 0;Serial.print("Page: ");Serial.println("Home");}void page1PushCallback(void *ptr){CurrentPage = 1;Serial.print("Page: ");Serial.println("Living Room");}
void page2PushCallback(void *ptr){CurrentPage = 2;Serial.print("Page: ");Serial.println("Bedroom");}void page3PushCallback(void *ptr){CurrentPage = 3;Serial.print("Page: ");Serial.println("Bathroom");}
void page4PushCallback(void *ptr){CurrentPage = 4;Serial.print("Page: ");Serial.println("Kitchen");}
NexTouch *nex_listen_list[] = {
  &L1,  &L2,  &L3,  &L4,  &L4,  &L6,  &L7,  &L8, &BD1,  &BD2,  &BD3,  &BD4,  &BD5,  &BD6,  &BD7,  &BD8,
  &T1,  &T2,  &T3,  &T4,  &T5,  &T6,  &T7,  &T8, &K1,  &K2,  &K3,  &K4,  &K5,  &K6,  &K7,  &K8,
  &page0,  &page1,  &page2,  &page3,  &page4, NULL };
struct Device {
  int deviceID;  char deviceType[3];  boolean location[7];  int dataRetentionTime;  bool locationEnabled;  bool presenceEnabled;  bool visualEnabled;  bool audioEnabled;  bool biometricsEnabled;
  bool healthEnabled;  bool usageEnabled;  bool environmentEnabled;  bool resourceOwnerAccess;  bool trustedPartyAccess;  bool serviceProviderAccess;  bool deviceManufacturerAccess;
  bool lawEnforcementAccess;  bool publicAccess;  bool thirdPartyAccess;  bool marketingOrganisationsAccess;  bool revenueUsage;  bool analyticsUsage;  bool researchUsage;  bool surveillanceUsage;
  bool securityUsage;  bool targetedAdsUsage;  bool lifeStyleUsage;  bool productivityUsage;};
Device devices[32];String LastStatus[32];
struct ColorMapping {    const char* deviceType;    int issueLED;    char color; };
ColorMapping mappings[] = {    {"LT", 18, 'G'}, {"TM", 18, 'G'}, {"VD", 18, 'G'}, {"VD", 22, 'R'},    {"VD", 17, 'R'}, {"VD", 19, 'R'}, {"VD", 10, 'R'}, {"VD", 9, 'R'},
    {"VD", 8, 'R'}, {"VD", 7, 'R'}, {"VD", 14, 'R'}, {"VD", 13, 'R'},    {"VD", 12, 'R'}, {"VD", 11, 'R'}, {"LK", 18, 'G'}, {"LK", 15, 'R'},
    {"LK", 22, 'R'}, {"LK", 17, 'R'}, {"LK", 19, 'R'}, {"LK", 10, 'R'},    {"LK", 9, 'R'}, {"LK", 8, 'R'}, {"LK", 7, 'R'}, {"LK", 14, 'R'},
    {"LK", 13, 'R'}, {"LK", 12, 'R'}, {"LK", 11, 'R'}, {"VA", 18, 'G'},    {"VA", 22, 'R'}, {"VA", 17, 'R'}, {"VA", 19, 'R'}, {"VA", 10, 'R'},
    {"VA", 9, 'R'}, {"VA", 13, 'R'}, {"VA", 12, 'R'}, {"VA", 11, 'R'},    {"VC", 18, 'G'}, {"TV", 18, 'G'}, {"TV", 15, 'R'}, {"TV", 22, 'R'},
    {"TV", 17, 'R'}, {"TV", 19, 'R'}, {"TV", 10, 'R'}, {"TV", 9, 'R'},    {"TV", 8, 'R'}, {"TV", 13, 'R'}, {"TV", 12, 'R'}, {"TV", 11, 'R'},
    {"AP", 18, 'G'}, {"FD", 18, 'G'}, {"FD", 15, 'R'}, {"FD", 22, 'R'},    {"FD", 17, 'R'}, {"FD", 19, 'R'}, {"FD", 10, 'R'}, {"FD", 9, 'R'},
    {"FD", 8, 'R'}, {"FD", 13, 'R'}, {"FD", 12, 'R'}, {"FD", 11, 'R'},    {"CM", 18, 'G'}, {"CM", 20, 'G'}, {"CM", 16, 'G'}, {"CM", 15, 'G'},
    {"CM", 17, 'G'}, {"CM", 19, 'G'}, {"CM", 10, 'G'}, {"CM", 9, 'G'},    {"CM", 8, 'G'}, {"CM", 13, 'G'}, {"CM", 12, 'G'}, {"CM", 11, 'G'},
    {"FN", 18, 'G'}, {"FT", 18, 'G'}, {"FT", 15, 'R'}, {"FT", 22, 'R'},    {"FT", 17, 'R'}, {"FT", 19, 'R'}, {"FT", 10, 'R'}, {"FT", 9, 'R'},
    {"FT", 8, 'R'}, {"FT", 13, 'R'}, {"FT", 12, 'R'}, {"FT", 11, 'R'},    {"SM", 18, 'G'}, {"BT", 18, 'G'}, {"SD", 18, 'G'}, {"TB", 18, 'G'},
    {"TB", 15, 'R'}, {"TB", 22, 'R'}, {"TB", 17, 'R'}, {"TB", 19, 'R'},    {"TB", 10, 'R'}, {"TB", 9, 'R'}, {"TB", 8, 'R'}, {"TB", 13, 'R'},
    {"TB", 12, 'R'}, {"TB", 11, 'R'}
};
void SetupLEDs(int DSBtn, bool fromnetwork){
  String deviceStatus=deviceToString(DSBtn);
  if (LastStatus[DSBtn]==deviceStatus && CurrentDevice==DSBtn && fromnetwork){Serial.println("No update for the device!");return;}
  else{
    btoff();
    if (DSBtn>=1 && DSBtn<=8 && CurrentPage!=1){changePage(1);CurrentPage=1;}else if(DSBtn>=9 && DSBtn<=16 && CurrentPage!=2){changePage(2);CurrentPage=2;}
    else if(DSBtn>=17 && DSBtn<=24 && CurrentPage!=3){changePage(3);CurrentPage=3;}else if(DSBtn>=25 && DSBtn<=32 && CurrentPage!=4){changePage(4);CurrentPage=4;}
    if (DSBtn==1){L1.setValue(1);}else if(DSBtn==2){L2.setValue(1);}else if(DSBtn==3){L3.setValue(1);}else if(DSBtn==4){L4.setValue(1);}
    else if(DSBtn==5){L5.setValue(1);}else if(DSBtn==6){L6.setValue(1);}else if(DSBtn==7){L7.setValue(1);}else if(DSBtn==8){L8.setValue(1);}
    else if(DSBtn==9){BD1.setValue(1);}else if(DSBtn==10){BD2.setValue(1);}else if(DSBtn==11){BD3.setValue(1);}else if(DSBtn==12){BD4.setValue(1);}
    else if(DSBtn==13){BD5.setValue(1);}else if(DSBtn==14){BD6.setValue(1);}else if(DSBtn==15){BD7.setValue(1);}else if(DSBtn==16){BD8.setValue(1);}
    else if(DSBtn==17){T1.setValue(1);}else if(DSBtn==18){T2.setValue(1);}else if(DSBtn==19){T3.setValue(1);}else if(DSBtn==20){T4.setValue(1);}
    else if(DSBtn==21){T5.setValue(1);}else if(DSBtn==22){T6.setValue(1);}else if(DSBtn==23){T7.setValue(1);}else if(DSBtn==24){T8.setValue(1);}
    else if(DSBtn==25){K1.setValue(1);}else if(DSBtn==26){K2.setValue(1);}else if(DSBtn==27){K3.setValue(1);}else if(DSBtn==28){K4.setValue(1);}
    else if(DSBtn==29){K5.setValue(1);}else if(DSBtn==30){K6.setValue(1);}else if(DSBtn==31){K7.setValue(1);}else if(DSBtn==32){K8.setValue(1);}
    off();
    CurrentDevice=DSBtn;
    LastStatus[DSBtn]=deviceToString(DSBtn);
  // Data Location and Retention
    for (int i=0; i<=6; i++){if (devices[DSBtn].location[i]){leds.setColorRGB(i, R, G, B);}}
    for (int i=0; i<=6; i++){if (devices[DSBtn].location[i]){leds.setColorRGB(i, R, G, B);}}
    for(int i=0; i<devices[DSBtn].dataRetentionTime; i++) {pixels.setPixelColor(i, pixels.Color(R, G, B));pixels.show();}
    // Data Type
    if (devices[DSBtn].locationEnabled){LocationBar.setBits(0x6);}if (devices[DSBtn].presenceEnabled){PressureBar.setBits(0x6);}
    if (devices[DSBtn].visualEnabled){VisualBar.setBits(0x6);}if (devices[DSBtn].audioEnabled){AudioBar.setBits(0x6);}
    if (devices[DSBtn].biometricsEnabled){BiometricBar.setBits(0x6);}if (devices[DSBtn].healthEnabled){HealthlBar.setBits(0x6);}
    if (devices[DSBtn].usageEnabled){UsageBar.setBits(0x6);}if (devices[DSBtn].environmentEnabled){EnviromentBar.setBits(0x6);}
    //Data Usage
    if (devices[DSBtn].surveillanceUsage){updateLEDColor(devices[DSBtn].deviceType, 7); }if (devices[DSBtn].researchUsage){updateLEDColor(devices[DSBtn].deviceType, 8);}
    if (devices[DSBtn].analyticsUsage){updateLEDColor(devices[DSBtn].deviceType, 9);}if (devices[DSBtn].revenueUsage){updateLEDColor(devices[DSBtn].deviceType, 10);}
    if (devices[DSBtn].productivityUsage){updateLEDColor(devices[DSBtn].deviceType, 11);}if (devices[DSBtn].lifeStyleUsage){updateLEDColor(devices[DSBtn].deviceType, 12);}
    if (devices[DSBtn].targetedAdsUsage){updateLEDColor(devices[DSBtn].deviceType, 13);}if (devices[DSBtn].securityUsage){updateLEDColor(devices[DSBtn].deviceType, 14);}
    // Data Access
    if (devices[DSBtn].deviceManufacturerAccess){updateLEDColor(devices[DSBtn].deviceType, 15);}if (devices[DSBtn].serviceProviderAccess){updateLEDColor(devices[DSBtn].deviceType, 16);}
    if (devices[DSBtn].thirdPartyAccess){updateLEDColor(devices[DSBtn].deviceType, 17);}if (devices[DSBtn].resourceOwnerAccess){updateLEDColor(devices[DSBtn].deviceType, 18);}
    if (devices[DSBtn].marketingOrganisationsAccess){updateLEDColor(devices[DSBtn].deviceType, 19);}if (devices[DSBtn].trustedPartyAccess){updateLEDColor(devices[DSBtn].deviceType, 20);}
    if (devices[DSBtn].publicAccess){updateLEDColor(devices[DSBtn].deviceType, 21);}if (devices[DSBtn].lawEnforcementAccess){updateLEDColor(devices[DSBtn].deviceType, 22);}
  }
}
void off(){
  LocationBar.setBits(0x0);  PressureBar.setBits(0x0);  VisualBar.setBits(0x0);  AudioBar.setBits(0x0);
  HealthlBar.setBits(0x0);  UsageBar.setBits(0x0);  EnviromentBar.setBits(0x0);  BiometricBar.setBits(0x0);
  for (int i=0; i<=22; i++){ leds.setColorRGB(i, 0, 0, 0);}for(int i=0; i<15; i++) { pixels.setPixelColor(i, pixels.Color(0, 0, 0));  pixels.show(); }
}
void btoff(){
    L1.setValue(0);L2.setValue(0);L3.setValue(0);L4.setValue(0);L5.setValue(0);L6.setValue(0);L7.setValue(0);L8.setValue(0);
    K1.setValue(0);K2.setValue(0);K3.setValue(0);K4.setValue(0);K5.setValue(0);K6.setValue(0);K7.setValue(0);K8.setValue(0);
    T1.setValue(0);T2.setValue(0);T3.setValue(0);T4.setValue(0);T5.setValue(0);T6.setValue(0);T7.setValue(0);T8.setValue(0);
    BD1.setValue(0);BD2.setValue(0);BD3.setValue(0);BD4.setValue(0);BD5.setValue(0);BD6.setValue(0);BD7.setValue(0);BD8.setValue(0);
}
void startUpTest(){
  LocationBar.setBits(0x1);  delay(500);  LocationBar.setBits(0x0);  PressureBar.setBits(0x1);  delay(500);  PressureBar.setBits(0x0); 
  VisualBar.setBits(0x1);    delay(500);  VisualBar.setBits(0x0);    AudioBar.setBits(0x1);     delay(500);  AudioBar.setBits(0x0);
  BiometricBar.setBits(0x1); delay(500);  BiometricBar.setBits(0x0); HealthlBar.setBits(0x1);   delay(500);  HealthlBar.setBits(0x0);
  UsageBar.setBits(0x1);     delay(500);  UsageBar.setBits(0x0);     EnviromentBar.setBits(0x1);delay(500);  EnviromentBar.setBits(0x0);
  for (int i=0; i<=6; i++){leds.setColorRGB(i, R, G, B);    delay(1000);    leds.setColorRGB(i, 0, 0, 0);}
  for (int i=7; i<=22; i++){leds.setColorRGB(i, R, G, B); delay(500); leds.setColorRGB(i, 0, 0, 0);}
  for(int i=0; i<15; i++) {pixels.setPixelColor(i, pixels.Color(R, G, B)); pixels.show(); delay(500);}
  for(int i=15; i>0; i--) {pixels.setPixelColor(i, pixels.Color(0, 0, 0));pixels.show();delay(500);}
  off();
}
bool CheckSign(const String& str, const String& prefix) {
    if (str.length() < prefix.length()) {return false;}
    for (size_t i = 0; i < prefix.length(); ++i) {
        if (str.charAt(i) != prefix.charAt(i)) {return false;}
    }return true;
}
void setup() {
  off();
  Serial.begin(9600);
  while (!Serial) {   ;  }
  if (WiFi.status() == WL_NO_MODULE) {Serial.println("Communication with WiFi module failed!");while (true);}
  String fv = WiFi.firmwareVersion();  if (fv < WIFI_FIRMWARE_LATEST_VERSION) {Serial.println("Please upgrade the firmware");}
  while (status != WL_CONNECTED) {Serial.print("Attempting to connect to WPA SSID: ");Serial.println(ssid);status = WiFi.begin(ssid, pass);delay(5000);}
  Serial.print("You're connected to the network");
  Serial1.begin(9600);
  nexInit(); btoff(); pixels.begin();server.begin();Serial.println("Server started");
  L1.attachPop(L1CB);L2.attachPop(L2CB);L3.attachPop(L3CB);L4.attachPop(L4CB);L5.attachPop(L5CB);L6.attachPop(L6CB);L7.attachPop(L7CB);L8.attachPop(L8CB);
  BD1.attachPop(BD1CB);BD2.attachPop(BD2CB);BD3.attachPop(BD3CB);BD4.attachPop(BD4CB);BD5.attachPop(BD5CB);BD6.attachPop(BD6CB);BD7.attachPop(BD7CB);BD8.attachPop(BD8CB);
  T1.attachPop(T1CB);T2.attachPop(T2CB);T3.attachPop(T3CB);T4.attachPop(T4CB);T5.attachPop(T5CB);T6.attachPop(T6CB);T7.attachPop(T7CB);T8.attachPop(T8CB);
  K1.attachPop(K1CB);K2.attachPop(K2CB);K3.attachPop(K3CB);K4.attachPop(K4CB);K5.attachPop(K5CB);K6.attachPop(K6CB);K7.attachPop(K7CB);K8.attachPop(K8CB);
  page0.attachPush(page0PushCallback);page1.attachPush(page1PushCallback);page2.attachPush(page2PushCallback);page3.attachPush(page3PushCallback);
  page4.attachPush(page4PushCallback);Serial.print("Device IP from DHCP: ");Serial.println(WiFi.localIP());
}
void loop() {
  WiFiClient client = server.available();
  if (!client) { nexLoop(nex_listen_list); delay(50); return;}
  while (client.connected() && !client.available()){delay(1);}
  String jsonBuffer = "";
  while(client.connected()){
    if (client.available()) {char c = client.read();
      if (c == '\n'){break;}
      jsonBuffer += c;}
  }
  if (isValidString(jsonBuffer)){
    client.println("OK");
    int dev = decodeStringToDevice(jsonBuffer);
    SetupLEDs(dev, true);
  }
  else{
    client.println("NA");Serial.println("Error in Incoming Packet:");
  }
  while (client.available()) {client.read();}
  client.stop();  nexLoop(nex_listen_list); delay(50);
}
void deviceStatusString(int dID){
  int index = dID;
  Serial.print(index);
  Serial.print(devices[index].deviceType);
  for (int i = 0; i < 7; i++) {
    Serial.print(devices[index].location[i]);
  }
  Serial.print(devices[index].dataRetentionTime);  Serial.print(devices[index].locationEnabled);
  Serial.print(devices[index].presenceEnabled);  Serial.print(devices[index].visualEnabled);
  Serial.print(devices[index].audioEnabled);  Serial.print(devices[index].biometricsEnabled);
  Serial.print(devices[index].healthEnabled);  Serial.print(devices[index].usageEnabled);
  Serial.print(devices[index].environmentEnabled);  Serial.print(devices[index].resourceOwnerAccess);
  Serial.print(devices[index].trustedPartyAccess);  Serial.print(devices[index].serviceProviderAccess);
  Serial.print(devices[index].deviceManufacturerAccess);  Serial.print(devices[index].lawEnforcementAccess);
  Serial.print(devices[index].publicAccess);  Serial.print(devices[index].thirdPartyAccess);
  Serial.print(devices[index].marketingOrganisationsAccess);  Serial.print(devices[index].revenueUsage);
  Serial.print(devices[index].analyticsUsage);  Serial.print(devices[index].researchUsage);
  Serial.print(devices[index].surveillanceUsage);  Serial.print(devices[index].securityUsage);
  Serial.print(devices[index].targetedAdsUsage);  Serial.print(devices[index].lifeStyleUsage);
  Serial.print(devices[index].productivityUsage);  Serial.println();
}
String deviceToString(int dID) {
  String result = "";
  result += String(devices[dID].deviceID);
  result += devices[dID].deviceType;
  for (int i = 0; i < 7; i++) {
    result += devices[dID].location[i] ? '1' : '0';
  }
  result += String(devices[dID].dataRetentionTime);
  result += devices[dID].locationEnabled ? '1' : '0';
  result += devices[dID].presenceEnabled ? '1' : '0';
  result += devices[dID].visualEnabled ? '1' : '0';
  result += devices[dID].audioEnabled ? '1' : '0';
  result += devices[dID].biometricsEnabled ? '1' : '0';
  result += devices[dID].healthEnabled ? '1' : '0';
  result += devices[dID].usageEnabled ? '1' : '0';
  result += devices[dID].environmentEnabled ? '1' : '0';
  result += devices[dID].resourceOwnerAccess ? '1' : '0';
  result += devices[dID].trustedPartyAccess ? '1' : '0';
  result += devices[dID].serviceProviderAccess ? '1' : '0';
  result += devices[dID].deviceManufacturerAccess ? '1' : '0';
  result += devices[dID].lawEnforcementAccess ? '1' : '0';
  result += devices[dID].publicAccess ? '1' : '0';
  result += devices[dID].thirdPartyAccess ? '1' : '0';
  result += devices[dID].marketingOrganisationsAccess ? '1' : '0';
  result += devices[dID].revenueUsage ? '1' : '0';
  result += devices[dID].analyticsUsage ? '1' : '0';
  result += devices[dID].researchUsage ? '1' : '0';
  result += devices[dID].surveillanceUsage ? '1' : '0';
  result += devices[dID].securityUsage ? '1' : '0';
  result += devices[dID].targetedAdsUsage ? '1' : '0';
  result += devices[dID].lifeStyleUsage ? '1' : '0';
  result += devices[dID].productivityUsage ? '1' : '0';
  return result;
}
void deviceStatus(int dID) {
  if (dID < 0 || dID >= 32) {Serial.println("Invalid device ID");return;}
  Serial.print("Printing Report of Device ID:"); Serial.print(dID);  Serial.print(", Type:");
  Serial.println(devices[dID].deviceType);  Serial.print("Locations: ");
  for (int i=0; i<=6; i++){Serial.print(i); Serial.print(":"); Serial.print(devices[dID].location[i] ? 'Y' : 'N');Serial.print(", ");}
  Serial.println();Serial.print("Retention Time:");
  Serial.println(devices[dID].dataRetentionTime);  Serial.print("Location Data Enabled:");
  Serial.print(devices[dID].locationEnabled ? 'Y' : 'N');  Serial.print(", Presense Data Enabled: ");
  Serial.print(devices[dID].presenceEnabled ? 'Y' : 'N');  Serial.print(", Vision Data Enabled: ");
  Serial.print(devices[dID].visualEnabled ? 'Y' : 'N');  Serial.print(", Audio Data Enabled: ");
  Serial.print(devices[dID].audioEnabled ? 'Y' : 'N');  Serial.print(", Biometrics Data Enabled: ");
  Serial.print(devices[dID].biometricsEnabled ? 'Y' : 'N');  Serial.print(", Health  Data Enabled :");
  Serial.print(devices[dID].healthEnabled ? 'Y' : 'N');  Serial.print(", Usage Data Enabled: ");
  Serial.print(devices[dID].usageEnabled ? 'Y' : 'N');  Serial.print(", Env Data Enabled: ");
  Serial.println(devices[dID].environmentEnabled ? 'Y' : 'N');  Serial.print("Resouse Owner Access: ");
  Serial.print(devices[dID].resourceOwnerAccess ? 'Y' : 'N');  Serial.print(", Trust Third Party Access :");
  Serial.print(devices[dID].trustedPartyAccess ? 'Y' : 'N');  Serial.print(", Service Provider Access :");
  Serial.print(devices[dID].serviceProviderAccess ? 'Y' : 'N');  Serial.print(", Device Manufacturer Access :");
  Serial.print(devices[dID].deviceManufacturerAccess ? 'Y' : 'N');  Serial.print(", Law Enforcement Access :");
  Serial.print(devices[dID].lawEnforcementAccess ? 'Y' : 'N');  Serial.print(", Public Access: ");
  Serial.print(devices[dID].publicAccess ? 'Y' : 'N');  Serial.print(", Third Party Access: ");
  Serial.print(devices[dID].thirdPartyAccess ? 'Y' : 'N');  Serial.print(", Marketing Organisations Access: ");
  Serial.println(devices[dID].marketingOrganisationsAccess ? 'Y' : 'N');  Serial.print("Revenue Usage: ");
  Serial.print(devices[dID].revenueUsage ? 'Y' : 'N');  Serial.print(", Analytics Usage: ");
  Serial.print(devices[dID].analyticsUsage ? 'Y' : 'N');  Serial.print(", Research Usage: ");
  Serial.print(devices[dID].researchUsage ? 'Y' : 'N');  Serial.print(", Surveillance Usage: ");
  Serial.print(devices[dID].surveillanceUsage ? 'Y' : 'N');  Serial.print(", Security Usage: ");
  Serial.print(devices[dID].securityUsage ? 'Y' : 'N');  Serial.print(", TargetedAds Usage: ");
  Serial.print(devices[dID].targetedAdsUsage ? 'Y' : 'N');  Serial.print(", LifeStyle Usage: ");
  Serial.print(devices[dID].lifeStyleUsage ? 'Y' : 'N');  Serial.print(", Productivity Usage: ");
  Serial.println(devices[dID].productivityUsage ? 'Y' : 'N');
}
bool isValidString(String str) {
  return true;
}
int decodeStringToDevice(String encodedStr) {
  int deviceID = encodedStr.substring(0, 2).toInt();
  char DT[3];  encodedStr.substring(2, 4).toCharArray(DT, sizeof(DT));  strncpy(devices[deviceID].deviceType, DT, sizeof(DT));
  byte locs = encodedStr.substring(4, 7).toInt();
  for (int i = 6; i >= 0; i--) {
    devices[deviceID].location[i] = bitRead(locs, i);
  }
  devices[deviceID].dataRetentionTime = encodedStr.substring(7, 9).toInt();
  byte dataEnabledByte = encodedStr.substring(9, 12).toInt();
  devices[deviceID].locationEnabled = bitRead(dataEnabledByte, 7);
  devices[deviceID].presenceEnabled = bitRead(dataEnabledByte, 6);
  devices[deviceID].visualEnabled = bitRead(dataEnabledByte, 5);
  devices[deviceID].audioEnabled = bitRead(dataEnabledByte, 4);
  devices[deviceID].biometricsEnabled = bitRead(dataEnabledByte, 3);
  devices[deviceID].healthEnabled = bitRead(dataEnabledByte, 2);
  devices[deviceID].usageEnabled = bitRead(dataEnabledByte, 1);
  devices[deviceID].environmentEnabled = bitRead(dataEnabledByte, 0);
  byte dataAccessByte = encodedStr.substring(12, 15).toInt();
  devices[deviceID].resourceOwnerAccess = bitRead(dataAccessByte, 7);
  devices[deviceID].trustedPartyAccess = bitRead(dataAccessByte, 6);
  devices[deviceID].serviceProviderAccess = bitRead(dataAccessByte, 5);
  devices[deviceID].deviceManufacturerAccess = bitRead(dataAccessByte, 4);
  devices[deviceID].lawEnforcementAccess = bitRead(dataAccessByte, 3);
  devices[deviceID].publicAccess = bitRead(dataAccessByte, 2);
  devices[deviceID].thirdPartyAccess = bitRead(dataAccessByte, 1);
  devices[deviceID].marketingOrganisationsAccess = bitRead(dataAccessByte, 0); 
  byte dataUsageByte = encodedStr.substring(15, 18).toInt();
  devices[deviceID].revenueUsage = bitRead(dataUsageByte, 7);
  devices[deviceID].analyticsUsage = bitRead(dataUsageByte, 6);
  devices[deviceID].researchUsage = bitRead(dataUsageByte, 5);
  devices[deviceID].surveillanceUsage = bitRead(dataUsageByte, 4);
  devices[deviceID].securityUsage = bitRead(dataUsageByte, 3);
  devices[deviceID].targetedAdsUsage = bitRead(dataUsageByte, 2);
  devices[deviceID].lifeStyleUsage = bitRead(dataUsageByte, 1);
  devices[deviceID].productivityUsage = bitRead(dataUsageByte, 0);
  return deviceID;
}
// void printCurrentNet() {
//   Serial.print("SSID: ");  Serial.println(WiFi.SSID());  byte bssid[6];  WiFi.BSSID(bssid);  Serial.print("BSSID: ");  printMacAddress(bssid);
//   long rssi = WiFi.RSSI();  Serial.print("signal strength (RSSI):");  Serial.println(rssi);  byte encryption = WiFi.encryptionType();
//   Serial.print("Encryption Type:");  Serial.println(encryption, HEX);  Serial.println();
// }
// void printMacAddress(byte mac[]) {
//   for (int i = 0; i < 6; i++) {    if (i > 0) {      Serial.print(":");    }    if (mac[i] < 16) {      Serial.print("0");    }    Serial.print(mac[i], HEX);  }  Serial.println();
// }
// void printWifiData() {
//   IPAddress ip = WiFi.localIP();  Serial.print("IP Address: ");  Serial.println(ip);  byte mac[6];  WiFi.macAddress(mac);  Serial.print("MAC address: ");  printMacAddress(mac);
// }
char getColor(const char* deviceType, int issueLED) {
    int numMappings = sizeof(mappings) / sizeof(mappings[0]);
    for (int i = 0; i < numMappings; i++) {if (strcmp(mappings[i].deviceType, deviceType) == 0 && mappings[i].issueLED == issueLED) {return mappings[i].color;}}
    return 'Y';
}
void updateLEDColor(const char* deviceType, int issueLED) {
    char color = getColor(deviceType, issueLED);
    switch(color) {
        case 'R':
            R = 255; G = 0; B = 0;
            break;
        case 'G':
            R = 0; G = 204; B = 0;
            break;
        default:
            R = 255; G = 255; B = 0;
            break;
    }
    leds.setColorRGB(issueLED, R, G, B);
}
void changePage(int pageId) {  Serial1.print("page ");  Serial1.print(pageId);  Serial1.write(0xFF);  Serial1.write(0xFF);  Serial1.write(0xFF);  Serial.println("Page changed to " + String(pageId));}